 /* -------------------------------------------------------------------------- */
 /*  AUTHOR: FAIZA A
 /* -------------------------------------------------------------------------- */
 /*  DATE: 15th March, 2021.
 /* -------------------------------------------------------------------------- */
 /*  COMPONENT DESCRIPTION: 
 /*  This component displays the medicine card inside the pharmacy
 /*  It is used to add items(medicines) to the cart-------------------------- */
 /* -------------------------------------------------------------------------- */
import React from 'react';
import {View, StyleSheet, Text, TextInput,TouchableWithoutFeedback, TouchableOpacity, Dimensions, Image, Button} from "react-native";
import TabletsIcon from '../assets/TabletsIcon';
import FontAwesome from 'react-native-vector-icons/FontAwesome';


const ItemCard = (props) => {
    return (
        <View style={{display:'flex', flexDirection:'row', justifyContent:'space-between', marginHorizontal:'4%',
        backgroundColor: '#fff', borderBottomColor:'#f3f3f3', borderBottomWidth:1, paddingVertical:'8%' }}>
        <View style={{display:'flex', flexDirection:'row'}}>
        <View style={{  }}>
        <TabletsIcon/>
        </View>
        {/* ----------------------------- Medicine details header ----------------------------- */}

        <View style={{display:'flex', flexDirection:'column', marginLeft: '5%'}}>
        <Text style={{fontWeight:'bold', fontSize: 14}}>Calpol 500 mg Tablet</Text>
        <Text style={{paddingTop:3,fontSize: 12, color:'#a1a1a1'}}>Strip of 15 tablets</Text>
        </View>
        </View>
        <View style={{display:'flex', flexDirection:'column'}}>
        <Text style={{fontSize: 12, color:'#0c0c0c', textAlign:'center'}}>₹30.74</Text>

        {/* ------------------------------- ADD button STARTS--------------------------------------------- */}
        {/* -------------------To be changed later to a button with counter values------------------- */}
        <View style={{flexDirection: 'row',alignItems: 'center'}}>
        <TouchableOpacity onPress={() => {}} activeOpacity={.7}>
        <Text style={{backgroundColor: '#48C2A5', color:'#fff', fontSize:14, fontWeight: 'bold', paddingVertical:5, paddingHorizontal: 15, borderRadius: 5, shadowColor: "#000", shadowOffset: {
                width: 0,
                height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
            elevation: 2.2,}}>ADD +</Text>
        </TouchableOpacity>
        </View>
        {/* ------------------------------- ADD button ENDS--------------------------------------------- */}
        </View>
        </View>
    )
}
export default ItemCard;