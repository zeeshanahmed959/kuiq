 /* -------------------------------------------------------------------------- */
 /*  AUTHOR: FAIZA A
 /* -------------------------------------------------------------------------- */
 /*  DATE: 15th March, 2021.
 /* -------------------------------------------------------------------------- */
 /*  COMPONENT DESCRIPTION: 
 /*  This component displays the medicine card inside the pharmacy
 /*  It is used to add items(medicines) to the cart-------------------------- */
 /* -------------------------------------------------------------------------- */
 import React from 'react';
 import {View, StyleSheet, Text, TextInput,TouchableWithoutFeedback, TouchableOpacity, Dimensions, Image, Button} from "react-native";
 import FontAwesome from 'react-native-vector-icons/FontAwesome';
 import Icon from 'react-native-vector-icons/Feather';
 
 const PastOrderCard = (props) => {
     return (
        <View style={{ marginTop:1, marginBottom:20,  borderBottomColor:'#f3f3f3', borderBottomWidth:1, borderTopColor:'#f3f3f3', borderTopWidth:1, height: 220, backgroundColor: '#fff'}}>
        {/* ----------------------------- Medicine details header ----------------------------- */}

         <View style={{ paddingHorizontal:'4%', marginTop:20, display:'flex', flexDirection:'column'}}>
         <View style={{display:'flex', flexDirection:'row', justifyContent:"space-between"}}>       
         <Text style={{fontWeight:'bold', fontSize: 16}}>Zee Pharmacy</Text>
         <TouchableOpacity onPress={() => {props.navigation.navigate('Cart');}} style={{display:'flex', flexDirection:'row',}}>
         <Text style={{paddingTop:3,fontSize: 10, color:'#000000', paddingRight:'2%'}}>Order Again</Text>
         <FontAwesome name='undo' size={15} style={{color :'#48C2A5', paddingTop:'1%'}}/>
         </TouchableOpacity>
         </View>
         <Text style={{paddingTop:3,fontSize: 12, color:'#a1a1a1'}}>Ballygunge</Text>
         </View>
         <View style={{borderBottomColor:'#f3f3f3', borderBottomWidth:1, paddingBottom:5, paddingHorizontal:'4%', display:'flex', flexDirection:'row', justifyContent:'space-between'}}>
         <Text style={{paddingTop:3,fontSize: 12, color:'#000000'}}>Bill amount</Text>
         <Text style={{paddingTop:3,fontSize: 12, color:'#000000'}}>₹66.74</Text>
         </View>
{/* -------------------------Cart Item details------------------------------------------------- */}
         <View style={{ paddingHorizontal:'4%', marginTop:10, display:'flex', flexDirection:'column', paddingBottom:5}}>
         <Text style={{fontWeight:'bold', fontSize: 14}}>Calpol 500 mg Tablet</Text>
         <Text style={{paddingTop:3,fontSize: 10, color:'#89898B', marginBottom:5}}>Strip of 15 tablets</Text>
         </View>
{/* --------------------------Order delivered date and Time----------------------------------------------- */}
         <View style={{display:'flex', flexDirection:'column', paddingHorizontal:'4%', height: 40, backgroundColor: '#e4f3fe', justifyContent:'center'}}>
         <Text style={{paddingTop:3,fontSize: 12, color:'#89898B'}}>Order delivered on October 7, 7:30 PM</Text>
         </View>
{/* ------------------------------RATE ORDER-------------------------------------- */}     
         <View style={{display:'flex', flexDirection:'row', justifyContent: 'space-evenly',
         width: '100%', height: 40, backgroundColor: '#fff',borderRadius:2, borderWidth:0.1, borderColor:'#a1a1a1', paddingTop:'2%'}}>
         <TouchableOpacity activeOpacity={.5}>
         <Text style={{paddingTop:3,fontSize: 14, color:'#48C2A5',fontWeight:'bold', textAlign:'center'}}>RATE ORDER</Text>
         </TouchableOpacity>
         <View style={{display:'flex', flexDirection:'row', justifyContent:'space-between', width:'50%'}}>

         <FontAwesome name='star' size={22} style={{color :'#FFC75A',}}/>
         <FontAwesome name='star' size={22} style={{color :'#FFC75A'}}/>
         <FontAwesome name='star' size={22} style={{color :'#FFC75A'}}/>
         <FontAwesome name='star' size={22} style={{color :'#FFC75A'}}/>
         <Icon name='star' size={22} style={{color :'#FFC75A'}}/>
         </View>
         </View>

{/* -------------------------------------------------------------------------- */}     
{/* ----This piece of code will come under an if/else structure. If Order is already rated, 
        Then this code becomes visble.  }
        {/* -------------------------------------------------------------------------- */} 
        {/*     <View activeOpacity={.5} style={{width: '100%', height: 40, backgroundColor: '#fff', justifyContent:'center'}}>
        <View style={{display:'flex', flexDirection:'column'}}>
        <Text style={{paddingTop:3,fontSize: 10, color:'#89898B', marginBottom:5, marginLeft:20, fontWeight: 'bold'}}>You rated this order</Text>
        <View style={{flexDirection:'row',width: 60, marginLeft:10}}>
        <View style={{paddingTop: 1, marginLeft:10}}>
        <StarIcon/></View>
        <Text style={{fontSize:12, color:'#a1a1a1', paddingLeft: 5}}>4.3/5  |</Text>
        <Text style={{fontSize: 12, color:'#89898B', marginBottom:5, marginLeft:5, fontWeight: 'bold'}}>Good</Text>
        </View>
        </View>
        </View>
        */}
        {/* -------------------------------------------------------------------------- */} 
        </View>
     )
 }
 export default PastOrderCard;