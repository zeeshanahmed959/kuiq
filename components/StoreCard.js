 /* -------------------------------------------------------------------------- */
 /*  AUTHOR: ZEESHAN A
 /* -------------------------------------------------------------------------- */
 /*  DATE: 5th March, 2021.
 /* -------------------------------------------------------------------------- */
 /*  PAGE DESCRIPTION: 
 /*  The page displays the a store card in the PHARMACY LIST on the Home Page
 /* -------------------------------------------------------------------------- */
import React from 'react';
import {View, StyleSheet, Text, TextInput, Dimensions, Image} from "react-native";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import PharmacyIcon from '../assets/PharmacyIcon';
import Icon from 'react-native-vector-icons/Feather';

const StoreCard = (props) => {
    return (
         <View style={{flexDirection:'row', justifyContent:'space-around', marginTop:'3%', marginHorizontal:'4%'}}>
         <View style={{width: '99%', height: 130, backgroundColor: '#fff', borderTopColor:'#f3f3f3', 
         borderTopWidth:1 ,shadowColor: "#000",
         shadowOffset: {
            width: 0,
            height: 2,
         },
         shadowOpacity: 0.25,
         shadowRadius: 3.84,
         elevation: 2.2, borderRadius: 10}}>
         {/* ----------------------PHARMACY ICON-------------------------------------------- */}
         <View style={{display:'flex', flexDirection:'row'}}>
         <View style={{width: 53, height: 53, marginLeft:10 , borderColor:'#ECECEC', 
         borderWidth: 1, borderRadius: 50, marginTop: 15}}>
         <PharmacyIcon/>
         </View>
         {/* ----------------------PHARMACY NAME/ADDRESS------------------------------------------ */}
         <View style={{marginLeft:8, marginTop:17, display:'flex', flexDirection:'column'}}>
         <Text style={{fontWeight:'bold', fontSize: 15}}>Random Pharmacy</Text>
         <Text style={{paddingTop:3,fontSize: 12, color:'#a1a1a1'}}>Ballygunge Area, 750m</Text>
         </View>
         </View>
         {/* ----------------------PHARMACY ICON-------------------------------------------- */}
         <View style={{flexDirection:'row'}}>
         <View style={{flexDirection:'row', justifyContent:'space-between', width:'100%', marginTop:15,paddingTop:12, borderTopWidth: 1, borderColor:'#f4f4f4'}}>
         <View style={{flexDirection:'row', justifyContent:'space-between'}}>
         <View style={{flexDirection:'row',width: 60, marginLeft:10}}>
         <View style={{paddingTop: 2}}><FontAwesome name="star" size={14} style={{color :'#FFC75A'}}/></View>
         <Text style={{fontSize:12, color:'#a1a1a1', paddingLeft: 5}}>4.3/5</Text>
         </View>
         <View style={{flexDirection:'row', width: 60, marginLeft: 8}}>
         <View style={{paddingTop:2}}><Icon name="clock" size={13} style={{color :'#4CAEF9'}}/></View>
         <Text style={{fontSize:12, color:'#a1a1a1', paddingLeft: 5}}>54mins</Text>
         </View>
         </View>
         <View style={{flexDirection: 'row', marginTop:-3}}>
         <Text style={{backgroundColor: '#f6665b', color:'#fff', fontSize:11, fontWeight: 'bold', paddingVertical:5, paddingHorizontal: 10, borderTopLeftRadius: 5, borderBottomLeftRadius: 5}}>Flat 10% OFF</Text>
         </View>
         </View>
         </View>
         {/* ---------------------- END OF BOTTOM BAR-------------------------------------------- */}
         </View>
         </View>
    )
}

export default StoreCard;