 /* -------------------------------------------------------------------------- */
 /*  AUTHOR: FAIZA A
 /* -------------------------------------------------------------------------- */
 /*  DATE: 16th March, 2021.
 /* -------------------------------------------------------------------------- */
 /*  COMPONENT DESCRIPTION: 
 /*  This component displays the medicine card inside the Cart
 /*  It is used to add/remove and check items(medicines) to the cart-------------------------- */
 /* -------------------------------------------------------------------------- */
 import React from 'react';
 import {View, StyleSheet, Text, TextInput,TouchableWithoutFeedback, TouchableOpacity, Dimensions, Image, Button} from "react-native";
 import TabletsIcon from '../assets/TabletsIcon';
 import FontAwesome from 'react-native-vector-icons/FontAwesome';
 
 
 const CartCard = (props) => {
     return (
         <View style={{display:'flex', flexDirection:'row', paddingHorizontal:'4%', backgroundColor: '#fff', justifyContent:'space-between', paddingVertical:10}}>
         <View style={{flexDirection:'row'}}>
         <View style={{marginTop:5}}>
         <TabletsIcon/>
         </View>
 {/* ----------------------------- Medicine details header ----------------------------- */}
 
         <View style={{marginLeft:15, display:'flex', flexDirection:'column'}}>
         <Text style={{fontWeight:'bold', fontSize: 14, color:'#010101'}}>Calpol 500 mg Tablet</Text>
         <Text style={{fontSize: 12, color:'#a1a1a1'}}>Strip of 15 tablets</Text>
         <Text style={{fontSize: 10, color:'#FF6F6F'}}>Prescription Required</Text>
         </View>
         </View>
         <View style={{display:'flex', flexDirection:'column'}}>
         <Text style={{fontSize: 12, color:'#000', textAlign:'center'}}>₹30.74</Text>
 
 {/* ------------------------------- ADD button STARTS--------------------------------------------- */}
 {/* -------------------To be changed later to a button with counter values------------------- */}
         <View style={{flexDirection: 'row'}}>
     <TouchableOpacity onPress={() => {}} activeOpacity={.7}>
        <Text style={{backgroundColor: '#fff', color:'#48C2A5', fontSize:13, fontWeight: 'bold', paddingVertical:5, paddingHorizontal: 15, borderRadius: 5, shadowColor: "#000", shadowOffset: {
             width: 0,
             height: 5,
          },
         shadowOpacity: 0.25,
         shadowRadius: 5,
         elevation: 2.7,}}>-  1  +</Text>
        </TouchableOpacity>
        </View>
 {/* ------------------------------- ADD button ENDS--------------------------------------------- */}
        </View>
        </View>
     )
 }
 export default CartCard;