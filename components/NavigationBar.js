import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import { NavigationContainer} from '@react-navigation/native';
import {createStackNavigator, CardStyleInterpolators} from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/Feather';
import Store from '../Screens/Store';
import Home from '../Screens/Home';
import Order from '../Screens/Order';
import Profile from '../Screens/Profile';
import Cart from '../Screens/Cart';
import Search from '../Screens/Search';
import PaymentMethod from '../Screens/PaymentMethod';
import OrderDetails from '../Screens/OrderDetails';
import AddLocation from '../Screens/AddLocation';
import TermsOfService from '../Screens/TermsOfService';
import About from '../Screens/About';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';
import Address from '../Screens/Address';
import Faq from '../Screens/Faq';
import Prescriptions from '../Screens/Prescriptions';
import PrescriptionView from '../Screens/PrescriptionView';
import AddressDetails from '../Screens/AddressDetails';
import Feedback from '../Screens/Feedback';

const PrescriptionsStack = createStackNavigator();
function PrescriptionsStackScreen() {
 return (
   <PrescriptionsStack.Navigator
    screenOptions={{
        cardStyleInterpolator: CardStyleInterpolators.forNoAnimation
        }}>
    <PrescriptionsStack.Screen name="Prescriptions" component={Prescriptions} options={{headerShown: false}}/>             
   <PrescriptionsStack.Screen name="PrescriptionView" component={PrescriptionView} options={{headerShown: false}} />
   </PrescriptionsStack.Navigator>
  );
}

const CartStack = createStackNavigator();
function CartStackScreen() {
 return (
   <CartStack.Navigator
    screenOptions={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
        }}>
    <CartStack.Screen name="Cart" component={Cart} options={{headerShown: false}}/>             
    <CartStack.Screen name="PaymentMethod" component={PaymentMethod} options={{headerShown: false}} />
    <CartStack.Screen name="OrderDetails" component={OrderDetailsStackScreen} options={{headerShown: false}} />
    <CartStack.Screen name="Prescriptions" component={PrescriptionsStackScreen} options={{headerShown: false}} />
   </CartStack.Navigator>
  );
}

const OrderDetailsStack = createStackNavigator();
function OrderDetailsStackScreen() {
    return (
      <OrderDetailsStack.Navigator
       screenOptions={{
           cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
           }}>
       <OrderDetailsStack.Screen name="OrderDetails" component={OrderDetails} options={{headerShown: false}}/>             
      </OrderDetailsStack.Navigator>
     );
   }

const StoreStack = createStackNavigator();
function StoreStackScreen() {
 return (
   <StoreStack.Navigator
    screenOptions={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
        }}>
    <StoreStack.Screen name="Store" component={Store} options={{headerShown: false}}/>             
    <StoreStack.Screen name="Cart" component={CartStackScreen} options={{headerShown: false}} />
    </StoreStack.Navigator>
  );
}
const AboutStack = createStackNavigator();
function AboutStackScreen(){
 return (
   <AboutStack.Navigator
    screenOptions={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
        }}>
    <AboutStack.Screen name="About" component={About} options={{headerShown: false}}/>             
    <AboutStack.Screen name="TermsOfService" component={TermsOfService} options={{headerShown: false}} />
    </AboutStack.Navigator>
  );
}

const AccountStack = createStackNavigator();
function AccountStackScreen({navigation, route}) {
    React.useLayoutEffect(() => {
    const routeName = getFocusedRouteNameFromRoute(route);
    if(routeName === "About" || routeName === "PaymentMethod" || routeName === "Address"
    || routeName === "Faq" || routeName === "Feedback"){
        navigation.setOptions({tabBarVisible: false});
    }else{
        navigation.setOptions({tabBarVisible: true});
    }},[navigation, route]);
 return (
   <AccountStack.Navigator
    screenOptions={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
        }}>
    <AccountStack.Screen name="Profile" component={Profile} options={{headerShown: false}}/>             
    <AccountStack.Screen name="PaymentMethod" component={PaymentMethod} options={{headerShown: false}} />
    <AccountStack.Screen name="Address" component={Address} options={{headerShown: false}} />
    <AccountStack.Screen name="Faq" component={Faq} options={{headerShown: false}} />
    <AccountStack.Screen name="About" component={AboutStackScreen} options={{headerShown: false}} />
    <AccountStack.Screen name="Feedback" component={Feedback} options={{headerShown: false}} />
    </AccountStack.Navigator>
  );
}

const AddressStack = createStackNavigator();
function AddLocationStackScreen(){
 return (
   <AddressStack.Navigator
    screenOptions={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
        }}>
    <AddressStack.Screen name="Address" component={Address} options={{headerShown: false}}/>             
    <AddressStack.Screen name="AddLocation" component={AddLocation} options={{headerShown: false}} />
    <AddressStack.Screen name="AddressDetails" component={AddressDetails} options={{headerShown: false}} />
    </AddressStack.Navigator>
  );
}

const HomeStack = createStackNavigator();
function HomeStackScreen({navigation, route}) {
    React.useLayoutEffect(() => {
    const routeName = getFocusedRouteNameFromRoute(route);
    if(routeName === "Store" || routeName === "Search" ||  routeName === "AddLocation" ){
        navigation.setOptions({tabBarVisible: false});
    }else{
        navigation.setOptions({tabBarVisible: true});
    }},[navigation, route]);
 return (
   <HomeStack.Navigator
    screenOptions={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
        }}>
    <HomeStack.Screen name="Home" component={Home} options={{headerShown: false}}/>             
    <HomeStack.Screen name="Store" component={StoreStackScreen} options={{headerShown: false}} />
    <HomeStack.Screen name="Search" component={Search} options={{headerShown: false}} />
    <HomeStack.Screen name="AddLocation" component={AddLocationStackScreen} options={{headerShown: false}} />
    </HomeStack.Navigator>
  );
}

const OrderStack = createStackNavigator();
function OrderStackScreen({navigation, route}) {
    React.useLayoutEffect(() => {
    const routeName = getFocusedRouteNameFromRoute(route);
    if(routeName === "OrderDetails"|| routeName === "Cart"){
        navigation.setOptions({tabBarVisible: false});
    }else{
        navigation.setOptions({tabBarVisible: true});
    }},[navigation, route]);
 return (
   <OrderStack.Navigator
   screenOptions={{
      cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
    }}>
    <OrderStack.Screen name="Order" component={Order} options={{headerShown: false}}/>             
    <OrderStack.Screen name="OrderDetails" component={OrderDetails} options={{headerShown: false}} />
    <OrderStack.Screen name="Cart" component={Cart} options={{headerShown: false}} />
   </OrderStack.Navigator>
  );
}



const Tab = createBottomTabNavigator()
const NavigationBar = () =>{
    return (
        <NavigationContainer>
            <Tab.Navigator
            initialRouteName="Profile"
            tabBarOptions = {{
                
                keyboardHidesTabBar: true,
                activeTintColor: '#52BCA3',
                inactiveTintColor: '#9e9e9e',
                labelStyle:{
                    fontWeight: 'bold',
                    fontSize: 8,
                    paddingBottom: 7
                }
            }}
            >
            <Tab.Screen 
            name = "HOME"
            component={HomeStackScreen}
            options={{
                    tabBarIcon: ({ focused }) => (
                    <Icon name="home" size={20}
                    style={{
                        color: focused ? '#52BCA3' : '#9e9e9e' }}/>
                    ),
                }}
            />
             <Tab.Screen 
            name = "Search"
            component={Search}
            options={{
                    tabBarIcon: ({ focused }) => (
                    <Icon name="search" size={20}
                    style={{
                        color: focused ? '#52BCA3' : '#9e9e9e' }}/>
                    ),
                }}
            />
            <Tab.Screen name="ORDER" component={OrderStackScreen} 
                options={{
                    tabBarIcon: ({focused}) => (
                        <Icon name="shopping-bag" size={20}  
                        style={{
                            color: focused ?'#52BCA3' : '#9e9e9e' }}/>
                        
                    ),
                }}
            />
            <Tab.Screen name="ACCOUNT" component={AccountStackScreen} 
                options={{
                    tabBarIcon: ({focused}) => (
                    <Icon name="user" size={20} 
                    style={{
                        color: focused ? '#52BCA3' : '#9e9e9e' }}
                    />
                    
                    ),
                }}
            />
            </Tab.Navigator>
        </NavigationContainer>
    );
}

NavigationBar.navigationOptions={
    headerShown:false
 };

export default NavigationBar;