 /* -------------------------------------------------------------------------- */
 /*  AUTHOR: FAIZA A
 /* -------------------------------------------------------------------------- */
 /*  DATE: 23rd March, 2021.
 /* -------------------------------------------------------------------------- */
 /*  COMPONENT DESCRIPTION: 
 /*  This component displays the search results for the medicines
 /* -------------------------------------------------------------------------- */
 import React from 'react';
 import {View, StyleSheet, Text, TextInput,TouchableWithoutFeedback, TouchableOpacity, Dimensions, Image, Button} from "react-native";
 import TabletsIcon from '../assets/TabletsIcon';
 import FontAwesome from 'react-native-vector-icons/FontAwesome';
 
 
 const SearchListCard = (props) => {
     return (
          <View style={{display:'flex', flexDirection:'row', justifyContent:'space-between', PaddingHorizontal:'10%',
           backgroundColor: '#fff', paddingVertical:'1%' }}>
           <TouchableOpacity activeOpacity={.3} 
           style={{display: 'flex', flexDirection:'column', }}>
           <View style={{display: 'flex', flexDirection:'row',marginTop:10, marginbottom:10, 
           paddingBottom: 10, justifyContent:'space-between', paddingLeft:'9%'}}>
           <TabletsIcon/>
           <Text style={{marginLeft:20, color:'#000',fontSize: 16}}>Calpol 500mg Tablet</Text>
           </View>
           </TouchableOpacity> 
        </View>
     )
 }
 export default SearchListCard;