import React from 'react';
import {SvgXml} from 'react-native-svg';

const StarIcon = () => {
    const star = `<svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M7.5 11.783L2.865 14.211L3.75 9.07L0 5.428L5.182 4.678L7.5 0L9.818 4.677L15 5.427L11.25 9.069L12.135 14.21L7.5 11.783Z" fill="#FFC75A"/>
    </svg>
    `
    return (
       <SvgXml xml={star}/>
    )
}

export default StarIcon;