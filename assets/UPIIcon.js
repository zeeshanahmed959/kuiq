import React from 'react';
import {SvgXml} from 'react-native-svg';

const UPIIcon = () => {
    const upi = `<svg width="45" height="23" viewBox="0 0 120 60" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path fill-rule="evenodd" clip-rule="evenodd" d="M95.678 42.8998L110 29.8348L103.216 16.3188L95.678 42.8998Z" fill="#097939"/>
    <path fill-rule="evenodd" clip-rule="evenodd" d="M90.854 42.8998L105.176 29.8348L98.392 16.3188L90.854 42.8998Z" fill="#ED752E"/>
    <path fill-rule="evenodd" clip-rule="evenodd" d="M22.4101 16.4699L16.3801 37.9449L37.7871 38.0949L43.6671 16.4699H49.0941L42.0441 41.6099C41.7741 42.5699 40.7461 43.3499 39.7491 43.3499H12.3101C10.6461 43.3499 9.6601 42.0499 10.1101 40.4499L16.8341 16.4699H22.4101ZM88.5921 16.3199H94.0191L86.4811 43.3499H80.9011L88.5921 16.3199ZM49.6981 27.5819L76.8341 27.4319L78.6441 21.7249H51.0541L52.7121 16.4689L82.1121 16.1989C83.9421 16.1819 85.0321 17.5989 84.5501 19.3659L81.7801 29.4899C81.2971 31.2559 79.4201 32.6869 77.5901 32.6869H53.3161L50.4541 43.7999H45.1741L49.6981 27.5819Z" fill="#747474"/>
    </svg>`
     return (
       <SvgXml xml={upi}/>
    )
}

export default UPIIcon;