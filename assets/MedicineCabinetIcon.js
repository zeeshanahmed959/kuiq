import React from 'react';
import {SvgXml} from 'react-native-svg';

const MedicineCabinetIcon = () => {
    const med = `<svg width="25" height="25" viewBox="0 0 72 72" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M27.67 43.857H13.72V28.467L27.714 28.502L27.732 14.384H43.461V28.334L57.278 28.252V43.857H43.461V57.941H27.635L27.67 43.857Z" fill="#EA5A47"/>
    <path d="M27.67 43.857H13.72V28.467L27.714 28.502L27.732 14.384H43.461V28.334L57.278 28.252V43.857H43.461V57.941H27.635L27.67 43.857Z" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
    </svg>
    `
    return (
       <SvgXml xml={med}/>
    )
}

export default MedicineCabinetIcon
