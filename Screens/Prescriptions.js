 /* -------------------------------------------------------------------------- */
 /*  AUTHOR: FAIZA A
 /* -------------------------------------------------------------------------- */
 /*  DATE: 23rd March, 2021.
 /* -------------------------------------------------------------------------- */
 /*  PAGE DESCRIPTION: 
 /*  The page allows the user to add location
 /* -------------------------------------------------------------------------- */
 import React from 'react';
 import {SafeAreaView, View, StyleSheet, Text, TextInput, TouchableWithoutFeedback, TouchableOpacity, Image} from 'react-native';
 import { ScrollView } from 'react-native-gesture-handler';
 import Icon from 'react-native-vector-icons/Feather';
 import PrescriptionIcon from '../assets/PrescriptionIcon';
 import FontAwesome from 'react-native-vector-icons/FontAwesome';

 
const Prescriptions = props => {
  
     return (
        <SafeAreaView style={styles.screen}>
        <View style={{display: 'flex',backgroundColor: '#fff', flexDirection:'row',
        paddingTop:'10%', paddingHorizontal:'4%', borderBottomColor:'#ECECEC', borderBottomWidth:1, paddingBottom:'4%'}}>
        <TouchableOpacity onPress={() => {props.navigation.navigate('Cart');}} style={{paddingTop:2, paddingRight:10}}>       
        <Icon name="arrow-left" size={22} style={{color :'#48C2A5'}}/></TouchableOpacity>
        <PrescriptionIcon/>
        <Text style={{fontSize: 18, color: '#000000', fontWeight: 'bold', paddingLeft:5}}>Prescriptions</Text>
        </View>

        <ScrollView>
        <View style={{flexDirection:'row', width:'100%',justifyContent:'space-evenly'}}>
        <TouchableWithoutFeedback onPress={() => {props.navigation.navigate('PrescriptionView');}} >
        <View style={{width:'30%', paddingBottom:20}}>
        <View style={{justifyContent: 'center', alignItems: 'center',marginTop:10,
          flexDirection:'row', justifyContent:'space-around', borderColor:'#9d9d9d', 
          borderWidth:1, padding:5}}> 
         <Image source={require('../assets/prescription.png')} style={{width:'100%', height:150}}/>
         </View>
         </View>
         </TouchableWithoutFeedback>

         <TouchableWithoutFeedback onPress={() => {props.navigation.navigate('PrescriptionView');}} >
        <View style={{width:'30%', paddingBottom:20}}>
        <View style={{justifyContent: 'center', alignItems: 'center',marginTop:10,
          flexDirection:'row', justifyContent:'space-around', borderColor:'#9d9d9d', 
          borderWidth:1, padding:5}}> 
         <Image source={require('../assets/prescription.png')} style={{width:'100%', height:150}}/>
         </View>
         </View>
         </TouchableWithoutFeedback>

         <TouchableWithoutFeedback onPress={() => {props.navigation.navigate('PrescriptionView');}} >
        <View style={{width:'30%', paddingBottom:20}}>
        <View style={{justifyContent: 'center', alignItems: 'center',marginTop:10,
          flexDirection:'row', justifyContent:'space-around', borderColor:'#9d9d9d', 
          borderWidth:1, padding:5}}> 
         <Image source={require('../assets/prescription.png')} style={{width:'100%', height:150}}/>
         </View>
         </View>
         </TouchableWithoutFeedback>
        </View>

        <View style={{flexDirection:'row', width:'100%',justifyContent:'space-evenly'}}>
        <TouchableWithoutFeedback onPress={() => {props.navigation.navigate('PrescriptionView');}} >
        <View style={{width:'30%', paddingBottom:20}}>
        <View style={{justifyContent: 'center', alignItems: 'center',marginTop:10,
          flexDirection:'row', justifyContent:'space-around', borderColor:'#9d9d9d', 
          borderWidth:1, padding:5}}> 
         <Image source={require('../assets/prescription.png')} style={{width:'100%', height:150}}/>
         </View>
         </View>
         </TouchableWithoutFeedback>

         <TouchableWithoutFeedback onPress={() => {props.navigation.navigate('PrescriptionView');}} >
        <View style={{width:'30%', paddingBottom:20}}>
        <View style={{justifyContent: 'center', alignItems: 'center',marginTop:10,
          flexDirection:'row', justifyContent:'space-around', borderColor:'#9d9d9d', 
          borderWidth:1, padding:5}}> 
         <Image source={require('../assets/prescription.png')} style={{width:'100%', height:150}}/>
         </View>
         </View>
         </TouchableWithoutFeedback>

         <TouchableWithoutFeedback onPress={() => {props.navigation.navigate('PrescriptionView');}} >
        <View style={{width:'30%', paddingBottom:20}}>
        <View style={{justifyContent: 'center', alignItems: 'center',marginTop:10,
          flexDirection:'row', justifyContent:'space-around', borderColor:'#9d9d9d', 
          borderWidth:1, padding:5}}> 
         <Image source={require('../assets/prescription.png')} style={{width:'100%', height:150}}/>
         </View>
         </View>
         </TouchableWithoutFeedback>
        </View>

        <View style={{flexDirection:'row', width:'100%',justifyContent:'space-evenly'}}>
        <TouchableWithoutFeedback onPress={() => {props.navigation.navigate('PrescriptionView');}} >
        <View style={{width:'30%', paddingBottom:20}}>
        <View style={{justifyContent: 'center', alignItems: 'center',marginTop:10,
          flexDirection:'row', justifyContent:'space-around', borderColor:'#9d9d9d', 
          borderWidth:1, padding:5}}> 
         <Image source={require('../assets/prescription.png')} style={{width:'100%', height:150}}/>
         </View>
         </View>
         </TouchableWithoutFeedback>

         <TouchableWithoutFeedback onPress={() => {props.navigation.navigate('PrescriptionView');}} >
        <View style={{width:'30%', paddingBottom:20}}>
        <View style={{justifyContent: 'center', alignItems: 'center',marginTop:10,
          flexDirection:'row', justifyContent:'space-around', borderColor:'#9d9d9d', 
          borderWidth:1, padding:5}}> 
         <Image source={require('../assets/prescription.png')} style={{width:'100%', height:150}}/>
         </View>
         </View>
         </TouchableWithoutFeedback>

         <TouchableWithoutFeedback onPress={() => {props.navigation.navigate('PrescriptionView');}} >
        <View style={{width:'30%', paddingBottom:20}}>
        <View style={{justifyContent: 'center', alignItems: 'center',marginTop:10,
          flexDirection:'row', justifyContent:'space-around', borderColor:'#9d9d9d', 
          borderWidth:1, padding:5}}> 
         <Image source={require('../assets/prescription.png')} style={{width:'100%', height:150}}/>
         </View>
         </View>
         </TouchableWithoutFeedback>
        </View>

        <View style={{flexDirection:'row', width:'100%',justifyContent:'space-evenly'}}>
        <TouchableWithoutFeedback onPress={() => {props.navigation.navigate('PrescriptionView');}} >
        <View style={{width:'30%', paddingBottom:20}}>
        <View style={{justifyContent: 'center', alignItems: 'center',marginTop:10,
          flexDirection:'row', justifyContent:'space-around', borderColor:'#9d9d9d', 
          borderWidth:1, padding:5}}> 
         <Image source={require('../assets/prescription.png')} style={{width:'100%', height:150}}/>
         </View>
         </View>
         </TouchableWithoutFeedback>

         <TouchableWithoutFeedback onPress={() => {props.navigation.navigate('PrescriptionView');}} >
        <View style={{width:'30%', paddingBottom:20}}>
        <View style={{justifyContent: 'center', alignItems: 'center',marginTop:10,
          flexDirection:'row', justifyContent:'space-around', borderColor:'#9d9d9d', 
          borderWidth:1, padding:5}}> 
         <Image source={require('../assets/prescription.png')} style={{width:'100%', height:150}}/>
         </View>
         </View>
         </TouchableWithoutFeedback>

         <TouchableWithoutFeedback onPress={() => {props.navigation.navigate('PrescriptionView');}} >
        <View style={{width:'30%', paddingBottom:20}}>
        <View style={{justifyContent: 'center', alignItems: 'center',marginTop:10,
          flexDirection:'row', justifyContent:'space-around', borderColor:'#9d9d9d', 
          borderWidth:1, padding:5}}> 
         <Image source={require('../assets/prescription.png')} style={{width:'100%', height:150}}/>
         </View>
         </View>
         </TouchableWithoutFeedback>
        </View>
        
         {/* <View style={{marginHorizontal:'4%', borderBottomColor:'#ECECEC', borderBottomWidth:1, paddingBottom:20}}>
        <Text style={{fontWeight:'bold', paddingTop:15, fontSize:14}}>Dr Zeeshan Orthopedic</Text>
        <View style={{justifyContent: 'center', alignItems: 'center',marginTop:10,
          flexDirection:'row', justifyContent:'space-around', borderColor:'#9d9d9d', 
          borderWidth:1, padding:5}}> 
         <Image source={require('../assets/prescription.png')} style={{width:'100%', height:100}}/>
         </View>
         </View>

         <View style={{marginHorizontal:'4%', borderBottomColor:'#ECECEC', borderBottomWidth:1, paddingBottom:20}}>
        <Text style={{fontWeight:'bold', paddingTop:15, fontSize:14}}>Dr Zeeshan Orthopedic</Text>
        <View style={{justifyContent: 'center', alignItems: 'center',marginTop:10,
          flexDirection:'row', justifyContent:'space-around', borderColor:'#9d9d9d', 
          borderWidth:1, padding:5}}> 
         <Image source={require('../assets/prescription.png')} style={{width:'100%', height:100}}/>
         </View>
         </View>

         <View style={{marginHorizontal:'4%', borderBottomColor:'#ECECEC', borderBottomWidth:1, paddingBottom:20}}>
        <Text style={{fontWeight:'bold', paddingTop:15, fontSize:14}}>Dr Zeeshan Orthopedic</Text>
        <View style={{justifyContent: 'center', alignItems: 'center',marginTop:10,
          flexDirection:'row', justifyContent:'space-around', borderColor:'#9d9d9d', 
          borderWidth:1, padding:5}}> 
         <Image source={require('../assets/prescription.png')} style={{width:'100%', height:100}}/>
         </View>
         </View> */}
         
         </ScrollView>

        <TouchableOpacity   onPress={() => {}} style={{height:50,backgroundColor:'#ADADAD',
         borderRadius:5,elevation:5,marginTop:'3%', marginHorizontal:'4%', marginBottom:20, flexDirection:'row', justifyContent:'center',alignItems:'center'}}>
         <Icon name="camera" size={20} style={{color :'#fff', paddingRight: 10}}/>
         <Text style={{fontSize:16,color:'white',fontWeight:'600'}}>Upload Prescription</Text>
         </TouchableOpacity>
        </SafeAreaView>
     )
 }
 
 /* ----------------------------- Navigation Bar ----------------------------- */
 
 Prescriptions.navigationOptions={
     headerShown:false
  };
 
 
 const styles = StyleSheet.create({
     screen:{
         flex:1,
       backgroundColor:'#fff',   
     }
 });
 
 export default Prescriptions;
 
 
 