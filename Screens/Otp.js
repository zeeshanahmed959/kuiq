 /* -------------------------------------------------------------------------- */
 /*  AUTHOR: ZEESHAN A
 /* -------------------------------------------------------------------------- */
 /*  DATE: 5th March, 2021.
 /* -------------------------------------------------------------------------- */
 /*  PAGE DESCRIPTION: 
 /*  The page displays verification/OTP 
 /* -------------------------------------------------------------------------- */
import React from 'react';
import { StyleSheet,Button, Text, View, TextInput,TouchableOpacity,Image } from 'react-native';


const Otp = props => {
    return (
     <View style={styles.screen} >
         {/* -------------------------IMAGE------------------------------------------------- */}
         <Image style={{marginTop:-150,position:'absolute',marginLeft:-100}} source={require('../assets/login.png')}  />
         {/* -------------------------HEADER+ PHONE NUMBER------------------------------------------------- */}
         <View style={{marginTop:'45%',marginLeft:'8%'}}>
         <Text style={{fontSize:24,fontWeight:'700'}}>You're almost there</Text>
         <Text style={{width:268,color:'#89898b',fontSize:16,fontWeight:'400'}}>Please enter the verification code sent to +91 7059869677</Text>
         <TouchableOpacity>
         <Text style={{color:'#50bea4',fontSize:16,fontWeight:'400'}}>change number</Text>
         </TouchableOpacity>
         </View>
         {/* -------------------------OTP CODE CIRCLES------------------------------------------------- */}
         <View style={{flexDirection:'row',marginTop:'8%',justifyContent:'space-evenly',marginLeft:'12%',marginRight:'12%'}}>
         <TextInput style={styles.input} maxLength={1} keyboardType='number-pad' returnKeyType='next' />
         <TextInput style={styles.input} maxLength={1} keyboardType='number-pad' />
         <TextInput style={styles.input} maxLength={1} keyboardType='number-pad' />
         <TextInput style={styles.input} maxLength={1} keyboardType='number-pad' />
         </View>
         {/* -------------------------VERIFY BUTTON------------------------------------------------- */}
         <TouchableOpacity   onPress={() => {
         props.navigation.navigate({routeName: 'NavigationBar'});}} style={{height:50,backgroundColor:'#48c2a5',borderRadius:25,elevation:5,margin:'7%',justifyContent:'center',alignItems:'center',marginBottom:4}}>
         <Text style={{fontSize:16,color:'white',fontWeight:'600'}}>Verify</Text>
         </TouchableOpacity>
         {/* -------------------------RESEND------------------------------------------------- */}
         <View style={{alignItems:'center',}}>
         <Text style={{color:'#89898b',fontSize:16,fontWeight:'400'}}>Did not receive the code?</Text>
         <TouchableOpacity>
         <Text style={{fontSize:16,color:'#50bea4',fontWeight:'400'}}>Resend</Text>
         </TouchableOpacity>
         </View>
    </View>);
};
{/* -------------------------NAVIGATION BAR SETTINGS------------------------------------------------- */} 
 Otp.navigationOptions={
   headerShown:false
 };
 const styles = StyleSheet.create({
    screen:{
        flex:1,
      backgroundColor:'#ffffff',       
    },
    input:{
        height:50,
        width:53,
        backgroundColor:'white',
        borderRadius:25,
        borderWidth:1,
        borderColor:'#f3f3f3',
        textAlign:'center',
        fontSize:16,
        fontWeight:'700'
    }

});

export default Otp;