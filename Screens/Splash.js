 /* -------------------------------------------------------------------------- */
 /*  AUTHOR: JAYANT 
 /* -------------------------------------------------------------------------- */
 /*  DATE: Sometime in 2020
 /* -------------------------------------------------------------------------- */
 /*  PAGE DESCRIPTION: 
 /*  The page displays the Onboarding screens.
 /* -------------------------------------------------------------------------- */
import React from 'react';
import { StyleSheet, Text, View,Image,TouchableOpacity } from 'react-native';
import { Pages } from 'react-native-pages';


const Splash = props=> {
  return (
    <View style={styles.container}>
   
    <Pages  
       indicatorPosition='bottom'
       indicatorColor='#50BEA4'
       indicatorOpacity={0.2} >
        {/* ------------------------------ FIRST SPLASH SCREEN--------------------------------- */}
       <View style={{ flex: 1, marginTop:'25%' }} >    
       <Image style={{position:'absolute',right:0,}} source={require('../assets/back_1.png')}  />
       <Text style={{fontSize:22,fontWeight:'700',textAlign:'left',width:'70%',marginTop:40, paddingHorizontal:'4%'}}>DISCOVER MEDICINE STORES NEAR YOU</Text>
       <Text style={{fontSize:18,fontWeight:'400',textAlign:'left',width:'70%',marginTop:5, paddingHorizontal:'4%'}}>We make it simple for you to order medicines from the trusted stores near you.</Text>
       <Image style={{position:'absolute', bottom:'20%',width:'100%'}} source={require('../assets/discover.png')}  />
       </View>
       {/* ------------------------------ SECOND SPLASH SCREEN--------------------------------- */}
       <View style={{ flex: 1, marginTop:'30%'}} >  
       <Image style={{position:'absolute',width:'100%'}} source={require('../assets/back_2.png')}  />
       <Image style={{marginTop:-10,width:'100%'}} source={require('../assets/search.png')}  />
       <Text style={{fontSize:22,fontWeight:'700',textAlign:'left',width:'70%',marginTop:15,paddingHorizontal:'4%'}}>FIND MEDICINES AND MORE</Text>
       <Text style={{fontSize:18,fontWeight:'400',textAlign:'left',width:'70%',marginTop:5,paddingHorizontal:'4%'}}>When you order from us, you get great deals and exclusive offers.</Text> 
       </View>
       {/* ------------------------------ THIRD SPLASH SCREEN--------------------------------- */}
       <View style={{ flex: 1, marginTop:'15%'}} >  
       <Image style={{position:'absolute',width:'100%',}} source={require('../assets/back_3.png')}  />
       <Text style={{fontSize:24,fontWeight:'700',textAlign:'left',width:'70%',marginTop:'18%',paddingHorizontal:'4%'}}>QUICK DELIVERY AT YOUR DOORSTEP</Text>
       <Text style={{fontSize:18,fontWeight:'400',textAlign:'left',width:'70%',marginTop:5,paddingHorizontal:'4%'}}>We make ordering medicine fast, hassle-free and at your doorstep.</Text> 
       <Image style={{marginTop:-10,width:'100%',right:-5,height:'54%'}} source={require('../assets/delivery.png')}  />
       <TouchableOpacity   onPress={() => {
       props.navigation.navigate({routeName: 'Login'});}} style={{height:50,backgroundColor:'#48c2a5',borderRadius:25,elevation:5,margin:'7%',justifyContent:'center',alignItems:'center'}}>
       <Text style={{fontSize:16,color:'white',fontWeight:'600'}}>Get started</Text>
       </TouchableOpacity>
       </View>
       {/* ------------------------------ THIRD SPLASH SCREEN ENDS--------------------------------- */}
      </Pages>
    </View>
  );
}

Splash.navigationOptions={
    headerShown:false
  };
  

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
 
  },
});
export default Splash;