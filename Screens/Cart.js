 /* -------------------------------------------------------------------------- */
 /*  AUTHOR: FAIZA A
 /* -------------------------------------------------------------------------- */
 /*  DATE: 15th March, 2021.
 /* -------------------------------------------------------------------------- */
 /*  PAGE DESCRIPTION: 
 /*  The page is displayed when the User Navigates to the CART from the Nav Bar in the Home Screen/ 
 /*                            or clicks on View Cart in the Store Screen. 
 /* -------------------------------------------------------------------------- */
 import React from 'react';
 import { SafeAreaView, StyleSheet, Text, View,TouchableOpacity} from 'react-native';
 import Icon from 'react-native-vector-icons/Feather';
 import FontAwesome from 'react-native-vector-icons/FontAwesome';
 import CartCard from '../components/CartCard';
 import { FlatList } from 'react-native-gesture-handler';
 import { ScrollView } from 'react-native-gesture-handler';
 
 const Cart = props=> {
   return (
 
     <SafeAreaView style={styles.screen}>
       
     {/*--------------------------------------------------------------------------- */
     /* ----------------------------BACK BUTTON------------------------------ */}
         
     <View style={{backgroundColor: '#fff', display: 'flex', 
     flexDirection: 'row', paddingTop: '10%', paddingHorizontal:'4%', paddingBottom:10}}>
     <TouchableOpacity onPress={() => {props.navigation.navigate('Store');}}>       
     <Icon name="arrow-left" size={22} style={{color :'#48C2A5'}}/></TouchableOpacity>
     </View>
     {/*--------------------------------------------------------------------------- */
     /* ----------------------------HEADER DATA START------------------------------ */
     /*                Location and Name of the Pharmacy.               */
     /* --------------------------------------------------------------------------  */}
     <ScrollView>
     <View style={{display: 'flex', flexDirection:'column'}}>
     <View style={{display: 'flex', flexDirection:'column',paddingHorizontal: '4%',backgroundColor:'#fff', paddingTop:'3%'}}>
     <Text style={{fontSize: 18, color: '#000000', fontWeight: 'bold'}}>Faiza Pharmacy</Text>
     <Text style={{fontSize: 12, color: '#a1a1a1', paddingBottom:'2%'}}>Ballygunge</Text>
     </View>
 
     {/*--------------------------------------------------------------------------- */
     /* ----------------------------ORDER CART CARDS  -------------------------------------- */}
 
     
     <CartCard/>
     <CartCard/>
     <CartCard/>
     
     
 
     </View>
        {/*--------------------------------------------------------------------------- */
        /* ----------------------------SELECT COUPON  -------------------------------------- */}
        <TouchableOpacity>
         <View style={{paddingVertical:15, display: 'flex', flexDirection:'row', backgroundColor: '#e4f3fe',paddingHorizontal:'4%'}}>
         <FontAwesome name='certificate' size={20} style={{color :'#4CAEF9'}}/>
        <Text style={{fontSize: 14, color: '#066ab6',marginLeft:10, fontWeight:'bold'}}>APPLY COUPON</Text>
        {/* NOT REQUIRED NOW <Text style={{fontWeight:'bold',fontSize: 14, color: '#066ab6', paddingBottom:'1%',marginTop:'2.5%', marginLeft:'40%'}}>APPLIED</Text> */}
         </View>
         </TouchableOpacity>
        {/*--------------------------------------------------------------------------- */
        /* ----------------------------BILLING DETAILS -------------------------------------- */}
       <View style={{display: 'flex', flexDirection:'column', backgroundColor: '#fff',
         paddingTop:'2%',  paddingHorizontal:'4%'}}>
        <Text style={{fontWeight:'bold', fontSize: 14, color:'#010101'}}>Bill Details</Text>
        <View style={{display: 'flex', flexDirection:'row', marginTop:'4%', justifyContent:'space-between'}}>
        <Text style={{fontSize: 12, color:'#000'}}>Item Total</Text>
        <Text style={{paddingTop:'0.2%',fontSize: 12, color:'#000'}}>₹30.74</Text>
        </View>
        <View style={{display: 'flex', flexDirection:'row', paddingBottom:'5%', marginBottom:'2%',borderBottomColor: '#ECECEC', 
        borderBottomWidth: 1, justifyContent:'space-between'}}>
        <Text style={{ fontSize: 12, color:'#4CAEF9'}}>Convenience fee</Text>
        <Text style={{ fontSize: 12, color:'#000'}}>₹35.00</Text>
        </View>
 
        <View style={{display: 'flex', flexDirection:'row', justifyContent:'space-between'}}>
        <Text style={{fontWeight:'bold', fontSize: 12, color:'#010101'}}>Grand Total</Text>
        <Text style={{fontWeight:'bold',fontSize: 12, color:'#010101'}}>₹30.74</Text>
        </View>
         {/*------------------------------SAVINGS MESSAGE--------------------------------------------- */}
        <View style={{display: 'flex', flexDirection:'row',backgroundColor: '#E5F9E1', paddingVertical:10,
         marginTop:'5%',borderWidth:0.7, borderColor:'#50BEA4', borderStyle:'dashed'}}>
        <Text style={{fontWeight:'bold', fontSize: 12, color:'#50BEA4', paddingLeft:'25%' }}>You have saved ₹10 on the bill!</Text>
        </View>
         
         {/*------------------------------ORDERING FOR--------------------------------------------- */
         /* Code not included in V1.0
         }
 
        <View style={{display: 'flex', flexDirection:'column', marginTop:'5%', backgroundColor:'#f8f8f8', paddingVertical:10, paddingHorizontal:10}}>
        <Text style={{fontSize: 12, color:'#000', paddingBottom:10}}>ORDERING FOR</Text>
        <View style={{display: 'flex', flexDirection:'row', justifyContent:'space-between'}}>
        <Text style={{fontWeight:'bold',fontSize: 14, color:'#3c3c3c'}}>Faiza Ahmad, 7003993853</Text>
        <Text style={{fontWeight:'bold',fontSize: 14, color:'#FF6F6F'}}>Change</Text>
        </View>
        </View>
         {/*------------------------------PRESCRIPTION--------------------------------------------- */}
 
        <View style={{display: 'flex', flexDirection:'row', backgroundColor:'#fff',marginTop:'5%', justifyContent:'space-between',
        paddingBottom:'5%', marginBottom:'5%', borderBottomColor: '#f8f8f8',borderBottomWidth: 3,}}>
        <View style={{flexDirection:'row'}}>
        <Text style={{color :'#FF6F6F', marginTop: -2}}>℞</Text>
        <Text style={{fontWeight:'bold',fontSize: 12, color:'#3c3c3c', marginLeft:'2%'}}>Medicines added requires prescription</Text>
        </View>
        <TouchableOpacity onPress={() => {props.navigation.navigate('Prescriptions');}}>
        <Text style={{fontWeight:'bold', paddingTop:'0.2%',fontSize: 12, color:'#FF6F6F'}}>Upload</Text>
        </TouchableOpacity>
        </View>
        {/*------------------------------REVIEW ORDER DISCLAIMER--------------------------------------------- */}
        
        <View style={{display: 'flex', flexDirection:'row', backgroundColor:'#fff',
        borderBottomColor: '#f8f8f8',borderBottomWidth: 3, paddingBottom:'5%'}}>
        <Icon name='list' size={15} style={{color :'#3c3c3c', paddingRight:5, marginTop:2}}/>
        <Text style={{fontWeight:'bold',fontSize: 12, color:'#3c3c3c', width:'80%'}}>Review your order and address details to avoid cancellation</Text>
        </View>
        {/*------------------------------NOTE--------------------------------------------- */}
        <View style={{display: 'flex', flexDirection:'row', backgroundColor:'#fff', paddingVertical:'5%'}}>
        <Text style={{fontWeight:'bold',fontSize: 12, color:'#FF6F6F',}}>NOTE: </Text>
        <Text style={{fontSize: 12, color:'#3c3c3c', width:'80%'}}>Orders placed in a wrong address cannot be redirected and will be cancelled. Items cannot be refunded or exchanged once you accept the deliverables.</Text>
        </View>
        </View>
       </ScrollView>
 
        {/*------------------------------DELIVER TO--------------------------------------------- */}
        
        <View style={{display: 'flex', flexDirection:'row', backgroundColor:'#f2ffe5', 
        paddingHorizontal:'4%', paddingVertical:'2%', justifyContent:'space-between'}}>
        <View style={{flexDirection:'row'}}>
        <FontAwesome name='check-circle' size={35} style={{color :'#48C2A5', paddingTop:4}}/>
        <View style={{display: 'flex', flexDirection:'column', marginLeft:10}}>
        <Text style={{fontSize: 12, color: '#a1a1a1'}}>Deliver To</Text>
        <Text style={{fontSize: 14, color: '#000', paddingBottom:'2%'}}>Ballygunge Area, Kolkata</Text>
        </View>
        </View>
        <Text style={{ paddingTop:'0.2%',fontSize: 14, color:'#FF6F6F', marginTop:20}}>Change</Text>
        </View>
        {/*----------------------PAYEMENT METHOD/ORDER NOW------------ */}
       
        <View style={{display: 'flex', flexDirection:'row'}}>
        <TouchableOpacity  onPress={() => {props.navigation.navigate('PaymentMethod');}} activeOpacity={.7} style={{display: 'flex', flexDirection:'column', backgroundColor:'#fff', 
        paddingVertical:10, width:'50%', alignItems:'center'}}>
        <View style={{display: 'flex', flexDirection:'row'}}>
        <Text style={{fontSize: 14, color: '#000'}}>Payment Method </Text>
        
        <FontAwesome name='credit-card' size={15} style={{color :'#000', marginTop:3}}/>
        {/* <FontAwesome name='caret-up' size={20} style={{color :'#000', marginLeft:'2%'}}/> */}
        </View>
        <Text style={{fontSize: 12, color: '#000', marginLeft:-35}}>4783XXXXXXX8762</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {props.navigation.navigate('OrderDetails');}} activeOpacity={.7} style={{display: 'flex', flexDirection:'row', 
        backgroundColor:'#FF7172', justifyContent:'center', width:'50%'}}>
        <Text style={{fontSize: 16, color: '#fff', paddingVertical:20}}>Order Now ₹75.74</Text>
        </TouchableOpacity>
        </View>
        
       {/* ---------------------------------- END PAYMENT METHOD ----------------------------------- */}
 
       </SafeAreaView>
 
     /* ---------------------------------- START ----------------------------------- */
  
   );
 }
 Cart.navigationOptions={
     headerShown:false
  };
 
 const styles = StyleSheet.create({
     screen:{
       flex:1,
       backgroundColor:'#f8f8f8',       
     }
 });
 export default Cart;