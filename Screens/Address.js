 /* -------------------------------------------------------------------------- */
 /*  AUTHOR: FAIZA A
 /* -------------------------------------------------------------------------- */
 /*  DATE: 23rd March, 2021.
 /* -------------------------------------------------------------------------- */
 /*  PAGE DESCRIPTION: 
 /*  The page allows the user to add location
 /* -------------------------------------------------------------------------- */
 import React from 'react';
 import {SafeAreaView, View, StyleSheet, Text, TextInput, TouchableWithoutFeedback, TouchableOpacity, Image} from 'react-native';
 import { ScrollView } from 'react-native-gesture-handler';
 import Icon from 'react-native-vector-icons/Feather';
 import FontAwesome from 'react-native-vector-icons/FontAwesome';

 
const Address = props => {
  
     return (
        <SafeAreaView style={styles.screen}>
        <View style={{display: 'flex',backgroundColor: '#fff', flexDirection:'row',
        marginTop:'10%', paddingHorizontal:'4%'}}>
        <TouchableOpacity onPress={() => {props.navigation.popToTop()}} style={{paddingTop:2}}>       
        <Icon name="arrow-left" size={22} style={{color :'#48C2A5'}}/></TouchableOpacity>
        <Text style={{fontSize: 18, color: '#000000', fontWeight: 'bold', paddingLeft:10}}>Search Location</Text>
        </View>

       
         <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center',
         backgroundColor: '#fff', borderWidth: 0.5, borderColor: '#ececec', 
         height: 50, borderRadius: 25, shadowColor: "#000", marginTop:10, marginHorizontal:'4%',
         shadowOffset: {
                 width: 0,
                 height: 3,
        },
         shadowOpacity: 0.25, shadowRadius: 4.4, elevation: 5,}}>
         <Icon name="search" size={21} style={{color :'#000000',padding: 10, paddingRight:5,
             margin: 4, alignItems: 'center',}}/>
           <TextInput
             style={{flex: 1}}
             placeholder="Search for your location..."
           />
         </View>
        <TouchableWithoutFeedback onPress={() => {props.navigation.navigate('AddLocation');}}>
         <View style={{flexDirection:'row', borderBottomColor:'#ECECEC', borderBottomWidth:1, paddingVertical:'2%', paddingHorizontal:'4%'}}>
         <Icon name="compass" size={21} style={{color :'#000000', paddingRight:5,
         margin: 4, alignItems: 'center', color :'#48C2A5'}}/>
         <Text style={{fontSize:16, color :'#48C2A5', paddingTop:3}}>Use current location</Text>
         </View>
         </TouchableWithoutFeedback>
         <Text style={{fontSize: 18, color: '#000000', fontWeight: 'bold', paddingHorizontal:'4%', paddingTop:'4%', paddingBottom:5}}>Saved Address</Text>
         <ScrollView>
         <TouchableOpacity onPress={() => {props.navigation.navigate('AddLocation');}}>
         <View style={{marginHorizontal:'4%', flexDirection:'row', paddingVertical:'4%', borderBottomColor:'#ECECEC', borderBottomWidth:1}}>
         <Icon name="map-pin" size={20} style={{color :'#9B9B9B', paddingTop:10}}/>
         <View style={{flexDirection:'column'}}>
         <Text style={{color:'#000', paddingLeft:10, fontSize:16, fontWeight:'bold'}}>Home</Text>
         <Text style={{color:'#909090', paddingLeft:10, fontSize:14}}>7, Ripon Lane, Kolkata 700016</Text>
         </View>
         </View>
         </TouchableOpacity>

         <View style={{marginHorizontal:'4%', flexDirection:'row', paddingVertical:'4%', borderBottomColor:'#ECECEC', borderBottomWidth:1}}>
         <Icon name="map-pin" size={20} style={{color :'#9B9B9B', paddingTop:10}}/>
         <View style={{flexDirection:'column'}}>
         <Text style={{color:'#000', paddingLeft:10, fontSize:16, fontWeight:'bold'}}>Home</Text>
         <Text style={{color:'#909090', paddingLeft:10, fontSize:14}}>7, Ripon Lane, Kolkata 700016</Text>
         </View>
         </View>

         <View style={{marginHorizontal:'4%', flexDirection:'row', paddingVertical:'4%', borderBottomColor:'#ECECEC', borderBottomWidth:1}}>
         <Icon name="map-pin" size={20} style={{color :'#9B9B9B', paddingTop:10}}/>
         <View style={{flexDirection:'column'}}>
         <Text style={{color:'#000', paddingLeft:10, fontSize:16, fontWeight:'bold'}}>Home</Text>
         <Text style={{color:'#909090', paddingLeft:10, fontSize:14}}>7, Ripon Lane, Kolkata 700016</Text>
         </View>
         </View>

         <View style={{marginHorizontal:'4%', flexDirection:'row', paddingVertical:'4%', borderBottomColor:'#ECECEC', borderBottomWidth:1}}>
         <Icon name="map-pin" size={20} style={{color :'#9B9B9B', paddingTop:10}}/>
         <View style={{flexDirection:'column'}}>
         <Text style={{color:'#000', paddingLeft:10, fontSize:16, fontWeight:'bold'}}>Home</Text>
         <Text style={{color:'#909090', paddingLeft:10, fontSize:14}}>7, Ripon Lane, Kolkata 700016</Text>
         </View>
         </View>

         <View style={{marginHorizontal:'4%', flexDirection:'row', paddingVertical:'4%', borderBottomColor:'#ECECEC', borderBottomWidth:1}}>
         <Icon name="map-pin" size={20} style={{color :'#9B9B9B', paddingTop:10}}/>
         <View style={{flexDirection:'column'}}>
         <Text style={{color:'#000', paddingLeft:10, fontSize:16, fontWeight:'bold'}}>Home</Text>
         <Text style={{color:'#909090', paddingLeft:10, fontSize:14}}>7, Ripon Lane, Kolkata 700016</Text>
         </View>
         </View>

         <View style={{marginHorizontal:'4%', flexDirection:'row', paddingVertical:'4%', borderBottomColor:'#ECECEC', borderBottomWidth:1}}>
         <Icon name="map-pin" size={20} style={{color :'#9B9B9B', paddingTop:10}}/>
         <View style={{flexDirection:'column'}}>
         <Text style={{color:'#000', paddingLeft:10, fontSize:16, fontWeight:'bold'}}>Home</Text>
         <Text style={{color:'#909090', paddingLeft:10, fontSize:14}}>7, Ripon Lane, Kolkata 700016</Text>
         </View>
         </View>

         <View style={{marginHorizontal:'4%', flexDirection:'row', paddingVertical:'4%', borderBottomColor:'#ECECEC', borderBottomWidth:1}}>
         <Icon name="map-pin" size={20} style={{color :'#9B9B9B', paddingTop:10}}/>
         <View style={{flexDirection:'column'}}>
         <Text style={{color:'#000', paddingLeft:10, fontSize:16, fontWeight:'bold'}}>Home</Text>
         <Text style={{color:'#909090', paddingLeft:10, fontSize:14}}>7, Ripon Lane, Kolkata 700016</Text>
         </View>
         </View>

         <View style={{marginHorizontal:'4%', flexDirection:'row', paddingVertical:'4%', borderBottomColor:'#ECECEC', borderBottomWidth:1}}>
         <Icon name="map-pin" size={20} style={{color :'#9B9B9B', paddingTop:10}}/>
         <View style={{flexDirection:'column'}}>
         <Text style={{color:'#000', paddingLeft:10, fontSize:16, fontWeight:'bold'}}>Home</Text>
         <Text style={{color:'#909090', paddingLeft:10, fontSize:14}}>7, Ripon Lane, Kolkata 700016</Text>
         </View>
         </View>
         </ScrollView>
        </SafeAreaView>
     )
 }
 
 /* ----------------------------- Navigation Bar ----------------------------- */
 
 Address.navigationOptions={
     headerShown:false
  };
 
 
 const styles = StyleSheet.create({
     screen:{
         flex:1,
       backgroundColor:'#ffffff',   
     }
 });
 
 export default Address;
 
 
 