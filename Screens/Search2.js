 /* -------------------------------------------------------------------------- */
 /*  AUTHOR: FAIZA A
 /* -------------------------------------------------------------------------- */
 /*  DATE: 19th March, 2021.
 /* -------------------------------------------------------------------------- */
 /*  PAGE DESCRIPTION: 
 /*  The page displays the search list.
 /*  The user selects the pharmacy to buy medicines from here.
 /* -------------------------------------------------------------------------- */

 import React from 'react';
 import {SafeAreaView,View, StyleSheet, Text, TextInput, TouchableWithoutFeedback,  TouchableOpacity,Image} from 'react-native';
 import { ScrollView } from 'react-native-gesture-handler';
 import Icon from 'react-native-vector-icons/Feather';
 import StoreIcon from '../assets/StoreIcon';
 import StoreCard from '../components/StoreCard';
 
 const Home = props => {
     const item = 'value';
     return(
         <SafeAreaView style={styles.screen}>
 
         {/* --------------------BACK BUTTON--------------------------
         ------------------------GOES TO search page---------------- */}
         
         <TouchableOpacity onPress={() => {props.navigation.navigate('Search');}} style={{display: 'flex', flexDirection: 'row', marginTop: '10%', marginLeft: 10}}>
         <Icon name="arrow-left" size={22} style={{color :'#48C2A5'}}/>
         </TouchableOpacity>
                                    
         {/*--------------------------------------------------------------------- */}
         {/* -----------------------------SEARCH BAR-------------------------- */}
             <TouchableWithoutFeedback onPress={() => {props.navigation.navigate({routeName: 'Home'});}}>
             <View style={{ justifyContent: 'center', alignItems: 'center',flexDirection: 'row', marginTop:'5%', marginLeft:'5%', marginRight:'5%', backgroundColor: '#fff', borderWidth: 0.5, borderColor: '#ececec', 
             height: 50, borderRadius: 25, margin: 10, shadowColor: "#000",
             shadowOffset: {
                 width: 0,
                 height: 4,
             },
             shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 9,}}>
             <Icon name="search" size={21} style={{color :'#000000',padding: 10, paddingRight:5,
             margin: 4, alignItems: 'center',}}/>
             <TextInput
             style={{flex: 1,color :'#000000', fontSize:16}}
             placeholder="Search for medicines..."
             underlineColorAndroid="transparent"
           />
         </View>
         </TouchableWithoutFeedback>
 
         {/* ----------END OF SEARCH BAR------------------- */}
 
         {/* ----------END OF HEADER HOME PAGE--------------------------
         --------------CONTAINS BACK BUTTON AND SEARCH BAR---------- */}
 
         {/* ----------SCROLL VIEW------------------- */}
 
         <ScrollView style={{paddingBottom: '10%'}}>
         <View style={{flexDirection:'row', marginTop: '1%'}}>
         <View style={{marginHorizontal:'4%'}}>
         <StoreIcon/>
         </View>
         <Text style={{fontSize:16, color:'#0c0c0c', fontWeight:'bold', paddingLeft:0}}>Shop Calpol 650mg Tablet from</Text>
         </View>
         <TouchableWithoutFeedback onPress={() => {props.navigation.navigate('Store');}}>
         <View>
         <StoreCard/>
         </View>
         </TouchableWithoutFeedback>
         <StoreCard/>
         <StoreCard/>
         <StoreCard/>
         <StoreCard/>
         <StoreCard/>
         <View style={{paddingBottom:'5%'}}>
         </View>
         </ScrollView>
 
         {/* ----------END OF SCROLL VIEW------------------- */}
 
         </SafeAreaView>
     )
 }
 
 Home.navigationOptions={
     headerShown:false
  };
 
 const styles = StyleSheet.create({
     screen:{
         flex:1,
       backgroundColor:'#ffffff',       
     },
     input:{
         height:50,
         width:53,
         backgroundColor:'white',
         borderRadius:25,
         borderWidth:1,
         borderColor:'#f3f3f3',
         textAlign:'center',
         fontSize:16,
         fontWeight:'700'
     }
 
 });
 export default Home;