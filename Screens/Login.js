 /* -------------------------------------------------------------------------- */
 /*  AUTHOR: ZEESHAN A
 /* -------------------------------------------------------------------------- */
 /*  DATE: 5th March, 2021.
 /* -------------------------------------------------------------------------- */
 /*  PAGE DESCRIPTION: 
 /*  The page displays the Sign-In Page.
 /* -------------------------------------------------------------------------- */
 import React,{useState} from 'react';
 import { StyleSheet, Text, View, TextInput,Dimensions,TouchableOpacity,Image } from 'react-native';
 import * as firebase from 'firebase';
import apiKeys from '../config/keys';

try {
  if (apiKeys) {
    firebase.initializeApp(apiKeys);
  }
} catch (err) {
  // ignore app already initialized error on snack
}
 const Login = props => {



  return (
    <View style={styles.screen} >
      {/* -------------------------IMAGE------------------------------------------------- */}
        <Image style={{marginTop:-150,position:'absolute',marginLeft:-100}} source={require('../assets/login.png')}  />
      {/* -------------------------HEADER------------------------------------------------- */}  
        <View style={{marginTop:'65%',marginLeft:'8%'}}>
        <Text style={{fontSize:24,fontWeight:'700'}}>Welcome back</Text>
        <Text style={{width:300,color:'#89898b',fontSize:16,fontWeight:'400'}}>Enter your registered phone number to sign in.</Text>
        </View>
      {/* -------------------------ENTER PHONE NUMBER------------------------------------------------- */}  
        <View style={{marginLeft:'8%',flexDirection:'row',marginTop:'8%', width:'80%'}}>
        <View style={{height:50,width:'25%',backgroundColor:'white',borderRadius:25,
         borderWidth:1,borderColor:'#f3f3f3',flexDirection:'row',justifyContent:'space-evenly',alignItems:'center',
         marginRight:20}}> 
        <Image source={require('../assets/cyprus.png')}  />
        <Text style={{paddingRight:5}}>+91</Text>
        </View>
        <TextInput style={{height:50,width:'75%',backgroundColor:'white',borderRadius:25,borderWidth:1,
        borderColor:'#f3f3f3',paddingLeft:20}} placeholder='Phone Number' keyboardType='number-pad'/>
        </View>
      {/* -------------------------SIGN-IN BUTTON------------------------------------------------ */}  
        <TouchableOpacity onPress={() => {
        props.navigation.navigate({routeName: 'Otp'});}} style={{height:50,backgroundColor:'#48c2a5',
        borderRadius:25,elevation:5,margin:'7%',justifyContent:'center',alignItems:'center',marginBottom:4}}>
        <Text style={{fontSize:16,color:'white',fontWeight:'600'}}>Sign in</Text>
        </TouchableOpacity>
      {/* -------------------------TERMS OF SERVICE/PRIVACY POLICY------------------------------- */}  
        <View style={{alignItems:'center',}}>
        <Text style={{color:'#89898b',fontSize:12,textAlign:'center'}}>By continuing, you agree to our</Text>
        <View style={{flexDirection: 'row'}}>
        <TouchableOpacity>
        <Text style={{color:'#50BEA4',fontSize:12,textAlign:'center', paddingRight: 8}}>Terms of Service</Text>
        </TouchableOpacity>
        <TouchableOpacity>
        <Text style={{color:'#50BEA4',fontSize:12,textAlign:'center'}}>Privacy Policy</Text>
        </TouchableOpacity>
        </View>
        </View>
        {/* ------------------NOT INCLUDING FACEBOOK/GOOGLE+ LOGIN OR SIGN UP IN V1.0 ------------ */
         /* -------------------------------------------------------------------------------------- */}

      {/* -------------------------OR ------------------------------------------------------------ */}  
       <View style={{flexDirection:'row',margin:'5%',justifyContent:'center',alignItems:'center',marginTop:'12%'}}>
        <View style={{borderWidth:1,width:'40%',borderColor:'#ececec',height:1}}></View>
        <View style={{borderWidth:1,borderColor:'#ececec',height:38,width:38,borderRadius:38,alignItems:'center',justifyContent:'center'}}>
        <Text style={{fontSize:16,color:'#ececec',fontWeight:'400'}}>OR</Text>
        </View>
        <View style={{borderWidth:1,width:'40%',borderColor:'#ececec',height:1}}></View>
        </View>
      {/* -------------------------FACEBOOK/INSTAGRAM LOGIN------------------------------------------------- */}  

        {/*<View style={{flexDirection:'row',justifyContent:'space-around',marginTop:'3%',}}>
        <Image style={{marginLeft:'25%'}} source={require('../assets/facebook.png')}  />
        <TouchableOpacity onPress={()=>signInWithGoogleAsync()}>
        <Image style={{marginRight:'25%'}} source={require('../assets/google.png')}  />
        </TouchableOpacity>
        </View>
      {/* -------------------------CONNECT TO SIGN-UP PAGE------------------------------------------------- */}  
        <View style={{flex:1,flexDirection:'row',alignSelf:'center'}}>
        <Text style={{fontSize:16,color:'#89898b',fontWeight:'400'}}>Don't have an account?</Text>
        <Text onPress={() => {
        props.navigation.navigate({routeName: 'Signup'});}} style={{paddingLeft:5,fontSize:16,color:'#50BEA4',fontWeight:'400'}}>Sign up</Text>
        </View>
    </View>);
};
{/* -------------------------NAVIGATION BAR SETTINGS------------------------------------------------- */}  
Login.navigationOptions={
   headerShown:false
};
const styles = StyleSheet.create({
    screen:{
        flex:1,
      backgroundColor:'#ffffff',       
    },

});

export default Login;