 /* -------------------------------------------------------------------------- */
 /*  AUTHOR: FAIZA A
 /* -------------------------------------------------------------------------- */
 /*  DATE: 15th March, 2021.
 /* -------------------------------------------------------------------------- */
 /*  PAGE DESCRIPTION: 
 /*  The page displays the Order History for the User. 
 /*  Can be navigated from the Navigation Bar
 /* -------------------------------------------------------------------------- */
import React from 'react';
import {SafeAreaView, View, StyleSheet, Text,TouchableWithoutFeedback, TextInput, Dimensions, TouchableOpacity, Image} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import PastOrderCard from '../components/PastOrderCard';
import OngoingOrderCard from '../components/OngoingOrderCard';

const Order = props => {
    return (
        <SafeAreaView style={styles.screen}>
        <View style={{backgroundColor: '#f8f8f8'}}>

      {/*--------------------------------------------------------------------------- */
       /* ----------------------------HEADER DATA START------------------------------ */
       /*                               PAST ORDERS                                   */
       /* --------------------------------------------------------------------------  */}
        <View style={{display: 'flex',backgroundColor: '#fff', flexDirection:'column',
        borderBottomColor:'#f3f3f3', borderBottomWidth:1}}>
        <Text style={{fontSize: 18, color: '#000000', fontWeight: 'bold', marginTop:'10%', 
        marginHorizontal:'4%', paddingBottom: '4%'}}>Orders</Text>
        </View>
      {/* ------------------------- PAST ORDER SCROLL VIEW CARDS------------------------- */}
      
        <ScrollView>
        <View style={{marginHorizontal:'4%', paddingVertical:10}}>
        <Text style={{fontSize:16,color:'#89898B', fontWeight:'bold'}}>Ongoing Orders</Text>
        </View>
        <TouchableWithoutFeedback onPress={() => {props.navigation.navigate('OrderDetails');}}>
        <View>
        <OngoingOrderCard/>
        </View>
        </TouchableWithoutFeedback>
        <View style={{marginHorizontal:'4%', paddingVertical:10}}>
        <Text style={{fontSize:16,color:'#89898B', fontWeight:'bold'}}>Past Orders</Text>
        </View>
        <TouchableWithoutFeedback onPress={() => {props.navigation.navigate('OrderDetails');}}>
        <View>
        <PastOrderCard/>
        </View>
        </TouchableWithoutFeedback>
        <PastOrderCard/>
        <PastOrderCard/>
        <PastOrderCard/>
        <PastOrderCard/>
        <PastOrderCard/>
        <PastOrderCard/>
        <PastOrderCard/>
        <PastOrderCard/>
        <PastOrderCard/>
        <View style={{paddingBottom:'30%'}}>
        </View>
        </ScrollView>
     {/*---------------------------------------------------------------------------- */} 
     {/* --------LOGIC for cart screen when there are no ongoing orders ------------ */}
     {/*--------it will disapper or become invalid once an order is placed---------- */}
     {/* --------------------------------------------------------------------------- */}
     
     {/* -------------------------------------------------------------------------- 
     <View style={{ flex: 1, marginTop:'50%'}} >    
     <Image style={{marginLeft:'10%', alignItems:'center'}} source={require('../assets/CartEmpty.png')}  />
     <Text style={{fontSize:16,fontWeight:'700',textAlign:'center',marginTop:40}}>MEDICINES AT YOUR CONVENIENCE</Text>

     <Text style={{fontSize:12,textAlign:'center', marginTop:5,marginLeft:20,color:'#707070'}}>You have no past and ongoing orders</Text>
      
     <Text style={{fontSize:12,textAlign:'center',marginLeft:20, color:'#707070'}}>Add medicines from the store</Text>
    
     <TouchableOpacity  activeOpacity={.3} onPress={() => {
      props.navigation.navigate({routeName: 'Search'});}} style={{height:50,backgroundColor:'#F8F8FA',borderRadius:25,elevation:3,margin:'8%',justifyContent:'center',alignItems:'center', borderColor: '#48c2a5', borderWidth:1.5, shadowColor:'#48c2a5' }}>
      <Text style={{fontSize:16,color:'#48c2a5',fontWeight:'800'}}>FIND MEDICINES</Text>
     </TouchableOpacity>
     </View>
     */}
     {/* ---------------------------------- END --------------------------------- */
      /* --------LOGIC for cart screen when the cart is empty ------------------- */
      /* ------------------------------------------------------------------------ */}
        </View>
        </SafeAreaView>
    )
}
{/*--------------------------NAV BAR footer------------------------------------------ */}
Order.navigationOptions={
    headerShown:false
 };

const styles = StyleSheet.create({
    screen:{
      flex:1,
      backgroundColor:'#ffffff',       
    }
});

export default Order;