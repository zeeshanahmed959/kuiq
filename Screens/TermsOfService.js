/* -------------------------------------------------------------------------- */
 /*  AUTHOR: FAIZA A
 /* -------------------------------------------------------------------------- */
 /*  DATE: 23rd March, 2021.
 /* -------------------------------------------------------------------------- */
 /*  PAGE DESCRIPTION: 
 /*  The page allows the user to add location
 /* -------------------------------------------------------------------------- */
 import React from 'react';
 import {SafeAreaView, View, StyleSheet, Text, TouchableOpacity} from 'react-native';
 import {ScrollView} from 'react-native-gesture-handler';
 import Icon from 'react-native-vector-icons/Feather';
 
 
const TermsOfService = props => {
  
     return (
         <SafeAreaView style={styles.screen}>
         <View style={{paddingBottom:"2%"}}>
         {/* -----------------BACK BUTTON--------------------------------------------*/
         /* ------------------------------------------------------------------------ */}
         <View style={{display: 'flex', flexDirection: 'row',marginHorizontal:'4%', marginTop:'10%'}}>
             <View>
         <TouchableOpacity onPress={() => {props.navigation.popToTop()}}>
         <Icon name="arrow-left" size={26} style={{color :'#50BEA4'}}/>
         </TouchableOpacity>
         </View>
         <Text style={{fontSize:18, color:'#000', fontWeight:'bold', paddingLeft:'4%'}}>Terms of Service</Text>
         </View>
         <ScrollView>
             <View style={{padding:'3%'}}>
         <Text>Terms of Service
         Last updated on January 15, 2021

         I. Acceptance of terms
         Thank you for using Zomato. These Terms of Service (the "Terms") are intended to make you aware of your legal rights and responsibilities with respect to your access to and use of the Zomato website at www.zomato.com (the "Site") and any related mobile or software applications ("Zomato Platform") including but not limited to delivery of information via the website whether existing now or in the future that link to the Terms (collectively, the "Services").

        These Terms are effective for all existing and future Zomato customers, including but without limitation to users having access to 'restaurant business page' to manage their claimed business listings.

        Please read ts in advance of the scheduled booking time. The restaurant reserves the right to cance
XII. Advertising
Some of the Services are supported by advertising revenue and may display advertisements and promotions. These advertisements may be targeted to the content of information stored on the Services, queries made through the Services or other information. The manner, mode and extent of advertising by Zomato on the Services are subject to change without specific notice to you. In consideration for Zomato granting you access to and use of the Services, you agree that Zomato may place such advertising on the Services.
Part of the site may contain advertising information or promotional material or other material submitted to Zomato by third parties or Customers. Responsibility for ensuring that material submitted for inclusion on the Zomato Platform or mobile apps complies with applicable international and national law is exclusively on the party providing the information/material. Your correspondence or business dealings with, or participation in promotions of, advertisers other than Zomato found on or through the Zomato Platform and or mobile apps, including payment and delivery of related goods or services, and any other terms, conditions, warranties or representations associated with such dealings, shall be solely between you and such advertiser. Zomato will not be responsible or liable for any error or omission, inaccuracy in advertising material or any loss or damage of any sort incurred as a result of any such dealings or as a result of the presence of such other advertiser(s) on the Zomato Platform and mobile application.
For any information related to a charitable campaign ("Charitable Campaign") sent to Customers and/or displayed on the Zomato Platform where Customers have an option to donate money by way of (a) payment on a third party website; or (b) depositing funds to a third party bank account, Zomato is not involved in any manner in the collection or utilization of funds collected pursuant to the Charitable Campaign. Zomato does not accept any responsibility or liability for the accuracy, completeness, legality or reliability of any information related to the Charitable Campaign. Information related to the Charitable Campaign is displayed for informational purposes only and Customers are advised to do an independent verification before taking any action in this regard.
XIII. Additional Terms and Conditions for Customers using the various services offered by Zomato:
1. ONLINE ORDERING:
Zomato provides online ordering services by entering into contractual arrangements with restaurant partners (“Restaurant Partners”) and Stores (as defined below) on a principal-to-principal basis for the purpose of listing their menu items or the Products (as defined below) for online ordering by the Customers on the Zomato Platform.

The Customers can access the menu items or Products listed on the Zomato Platform and place online orders against the Restaurant Partner(s)/Store(s) through Zomato.

Your request to order food and beverages or Products from a Restaurant Partner or a Store page on the Zomato Platform shall constitute an unconditional and irrevocable authorization issued in favour of Zomato to place online orders for food and beverages or Products against the Restaurant Partner(s)/Store(s) on your behalf.

Delivery of an order placed by you through the Zomato Platform may either be undertaken directly by the Restaurant Partner or the Store against whom you have placed an order, or facilitated by Zomato through third-party who may be available to provide delivery services to you (“Delivery Partners”). In both these cases, Zomato is merely acting as an intermediary between you and the Delivery Partners, or you and the Restaurant Partner or the Store, as the case may be.

The acceptance by a Delivery Partner of undertaking delivery of your order shall constitute a contract of service under the Consumer Protection Act, 2019 or any successor legislations, between you and the Delivery Partner, to which Zomato is not a party under any applicable law. It is clarified that Zomato does not provide any delivery or logistics services and only enables the delivery of food and beverages or Products ordered by the Customers through the Zomato Platform by connecting the Customers with the Delivery Partners or the Restaurant Partners or the Store, as the case may be.

Where Zomato is facilitating delivery of an order placed by you, Zomato shall not be liable for any acts or omissions on part of the Delivery Partner including deficiency in service, wrong delivery of order, time taken to deliver the order, order package tampering, etc.

You may be charged a delivery fee for delivery of your order by the Delivery Partner or the Restaurant Partner or the Store, as the Delivery Partner or the Restaurant Partner or the Store may determine (“Delivery Charges"). You agree that Zomato is authorized to collect, on behalf of the Restaurant Partner or the Delivery Partner or the Store, the Delivery Charges for the delivery service provided by the Restaurant Partner or the Store or the Delivery Partner, as the case may be. The Delivery Charges may vary from order to order, which may be determined on multiple factors which shall include but not be limited to Restaurant Partner / Store, order value, distance, time of the day. Zomato will inform you of the Delivery Charges that may apply to you, provided you will be responsible for Delivery Charges incurred for your order regardless of your awareness of such Delivery Charges.

In addition to the Delivery Charges, you may also be charged an amount towards delivery surge for delivery of your order facilitated by the Delivery Partner or the Restaurant Partner or the Store, which is determined on the basis of various factors including but not limited to distance covered, time taken, demand for delivery, real time analysis of traffic and weather conditions, seasonal peaks or such other parameters as may be determined from time to time (“Delivery Surge"). You agree that Zomato is authorized to collect, on behalf of the Restaurant Partner or the Delivery Partner or the Store, the Delivery Surge for the delivery service provided by the Restaurant Partner or the Store or the Delivery Partner, as the case may be. The Delivery Surge may vary from order to order, which may be determined on multiple factors which shall include but not be limited to Restaurant Partner / Store, order value, distance, demand during peak hours. Zomato will use reasonable efforts to inform you of the Delivery Surge that may apply to you, provided you will be responsible for Delivery Surge incurred for your order regardless of your awareness of such Delivery Surge.


Online Ordering with Restaurant Partners:
All prices listed on the Zomato Platform are provided by the Restaurant Partner at the time of publication on the Zomato Platform and have been placed as received from the Restaurant Partner. While we take great care to keep them up to date, the final price charged to you by the Restaurant Partner may change at the time of delivery. In the event of a conflict between price on the Zomato Platform and price charged by the Restaurant Partner, the price charged by the Restaurant Partner shall be deemed to be the correct price except Delivery Charge of Zomato.
Pickup/Takeaway: When You opt for a Pickup/Takeaway (as defined below) You agree to be solely liable to ensure compliance with the conditions governing the Takeaway at the time of placing the Order, and Zomato shall not be liable in any manner in this regard. For the purpose of clarity, Pickup (in India)/Takeaway (in all other jurisdictions) would mean where a Restaurant Partner has agreed to provide an option to the Customers to collect the Order themselves from the Restaurant Partner on which such an Order is placed. Zomato accepts no liability associated with food preparation by the Restaurant Partner accepting the Order, and all food preparation and hand over through Takeaway are the sole responsibility of the Restaurant Partner accepting the Order. The Pickup/Takeaway times for collection are provided by the Restaurant Partner and are only estimates.
On Time or Free: For Customers in India, You may opt for on-time services offered by Zomato, for an additional non-refundable cost, at select Restaurant Partners. However You acknowledge that such services are facilitated by Zomato on a best effort basis, hence should your Order fail to reach you on or prior to the Promise Time, you would be eligible to claim and receive a Coupon worth Order Value or up-to INR 200, whichever is lower. You will be required to claim the Coupon within twenty four (24) hours from the time such Order is delivered to You failing which your eligibility to receive the Coupon will expire. Further the validity period of the Coupon would be 7 (seven) days from receipt thereof. Notwithstanding anything set out herein above, You shall not be eligible to receive the Coupon if:
Delay on the Promise Time is for unforeseen reasons eg. strikes, natural disaster, Restaurant Partner’s inability to provide the Order.
You change, edit, modify or cancel such Order or do any such act that has the effect of changing, editing or modifying such Order including but not limited to adding or changing the items ordered, receiving delivery at a location other than the one indicated at the time of placing of the Order etc.
You indulge in actions intended to delay the Order including but not limited to asking the Delivery Partner to delay the Order, becoming unresponsive on call etc.
The order is a bulk Order (as per Zomato’s standard order size)

For the purpose of this clause, words capitalized shall have the following meaning:
“Promise Time" shall mean the time period between the Restaurant Partner accepting the Order and the Delivery Partner reaching within 50 metre radius from Your location or first barrier point (security guard/reception etc.) whichever is further.
“Coupon" shall mean one- time code generated by Zomato for delay in Promise Time to be used as per applicable conditions.
Zomato Pro For Online Ordering:
As a member of Zomato Pro in India, you can avail Discount (as defined below) on Order placed by You from partnered Restaurants (“Partnered Restaurant(s)”).
On every Order placed by You through the Zomato Platform, You will get a percentage of discount on the Order value provided that the Order Value for such Order is above the minimum Order value specified by the Partnered Restaurant on the Zomato Platform (“Discount”).
The number of Partnered Restaurant(s) may be modified.
The Discount cannot be clubbed with any other offers or discounts or deals extended by the Partnered Restaurant or Zomato or any other third party.
The Discount is not valid on menu items sold by the Partnered Restaurant at maximum retail price (MRP), combos and special dishes.
The Discount can be availed only for Orders placed for home delivery.
The Partnered Restaurant(s) offering Zomato Pro for home delivery may differ from Restaurants offering Zomato Pro for dine out.
You will be responsible to pay the Partnered Restaurant(s) all costs and charges payable for all the other items for which you have placed an Order and are not covered under the Discount.
Zomato reserves the right to add exclusion days to Zomato Pro on Online Ordering at its discretion which will be communicated from time to time.
The term of your Zomato Pro membership shall be subject to the membership plan opted by You.
Exclusion Days for India
         </Text>
         </View>
         </ScrollView>
         </View>
         
     </SafeAreaView>
     )
 }
 
 /* ----------------------------- Navigation Bar ----------------------------- */
 
 TermsOfService.navigationOptions={
     headerShown:false
  };
 
 
 const styles = StyleSheet.create({
     screen:{
         flex:1,
       backgroundColor:'#ffffff',       
     }
 });
 
 export default TermsOfService;
 