 /* -------------------------------------------------------------------------- */
 /*  AUTHOR: FAIZA A
 /* -------------------------------------------------------------------------- */
 /*  DATE: 15th March, 2021.
 /* -------------------------------------------------------------------------- */
 /*  PAGE DESCRIPTION: 
 /*  The page displays the medicine cabinet for every Store.
 /*  The user can add the medicines to the cart from here.
 /* -------------------------------------------------------------------------- */
import React from 'react';
import {SafeAreaView, View, StyleSheet, Text, TextInput, TouchableWithoutFeedback, TouchableOpacity} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import ItemCard from '../components/ItemCard';
import MedicineCabinetIcon from '../assets/MedicineCabinetIcon';


const Store = props => {
  
    return (
        <SafeAreaView style={styles.screen}>
        <View style={{backgroundColor: '#e0f7fd', paddingBottom:"2%"}}>
        <TouchableOpacity onPress={() => {props.navigation.navigate('Home');}} 
        style={{display: 'flex', flexDirection: 'row', marginTop: '10%', marginHorizontal:'4%'}}>
        <Icon name="arrow-left" size={22} style={{color :'#48C2A5'}}/>
        </TouchableOpacity>
        </View>

        <ScrollView style={{paddingBottom: '10%'}}>
        <View style={{backgroundColor: '#e0f7fd',  borderBottomColor:'#ECECEC', borderBottomWidth:1}}>
       
        {/*--------------------------------------------------------------------------- */
        /* ----------------------------HEADER DATA ----------------------------------- */
        /*                 Rating, Location and Name of the Pharmacy.                  */
        /* --------------------------------------------------------------------------  */}

        <View style={{display: 'flex', flexDirection:'column',marginHorizontal:'4%'}}>
        <Text style={{fontSize: 18, color: '#0c0c0c', fontWeight: 'bold'}}>Zee Pharmacy</Text>
        <View style={{display: 'flex', flexDirection:'row', marginTop:'2%'}}>
        <FontAwesome name="star" size={15} style={{color :'#FFC75A', marginRight: 4}}/>
        <Text style={{fontWeight:'bold', fontSize: 12, color: '#000000', }}>4.7</Text>
        <Text style={{fontSize: 12, color: '#a1a1a1', marginRight: '1%' }}>/5</Text>
        <Text style={{fontSize: 13, color: '#a1a1a1'}}>| Ballygunge</Text>
        </View>
        </View>
        {/*--------------------------------------------------------------------------- */
        /* ----------------------------HEADER 2 -------------------------------------- */
        /*                 Coupon, Avg Delivery Time, distance                         */
        /* --------------------------------------------------------------------------  */}
        <View style={{display: 'flex', flexDirection: 'row', justifyContent:'space-between', marginHorizontal:'4%'}}>
        <View style={{backgroundColor: '#f6665b',color:'#fff', marginTop: 20, borderRadius:5, alignItems:'center'}}>
        <View style={{display: 'flex', flexDirection: 'column', paddingVertical:7, paddingHorizontal:10}}>
        <Text style={{fontWeight:'bold', fontSize: 14, color: '#fff', }}>Flat 15% OFF</Text>
        <View style={{display: 'flex', flexDirection: 'row',}}>
        <Text style={{fontSize: 12, color: '#fff' }}>orders above</Text>
        <Text style={{fontWeight:'bold', fontSize: 12, color: '#fff', }}> ₹150</Text></View>
        </View>
        </View>

        <View style={{display: 'flex', flexDirection:'column', marginTop: 20, marginBottom:15}}>
        <View style={{display: 'flex', flexDirection: 'row'}}>
        <Icon name="clock" size={12} style={{color :'#4CAEF9', marginTop:7}}/>
        <Text style={{fontSize: 12, color: '#4CAEF9', marginTop:5 }}> Avg. delivery time: 60 mins</Text></View>
        <View style={{display: 'flex', flexDirection: 'row'}}>
        <FontAwesome name="road" size={12} style={{color :'#89898B', marginTop:2}}/>
        <Text style={{ fontSize: 12, color: '#a1a1a1', }}> 800 meters away</Text></View>
        </View>
        </View> 
        {/*--------------------------------------------------------------------------- */
        /* ----------------------------SEARCH BAR ------------------------------------ */
        /* --------------------------------------------------------------------------  */}

        <View style={{ justifyContent: 'center', alignItems: 'center',
        flexDirection: 'row', backgroundColor: '#fff', borderWidth: 0.5, borderColor: '#ececec', 
        height: 50, borderRadius: 25, margin: 10, shadowColor: "#000",
        shadowOffset: {
                width: 0,
                height: 2,
            },
        shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 2,}}>
        <Icon name="search" size={21} style={{color :'#000000',padding: 10, paddingRight:5,
            margin: 4, alignItems: 'center',}}/>
        <TextInput
          style={{flex: 1}}
          placeholder="Search for medicines..."
          underlineColorAndroid="transparent"
        />
        </View>
        </View>
        
        {/* --------------------------- End of Blue portion -------------------------- */}
        {/* -----------------------    MEDICINE CABINET----------------------- */}

        <View style={{display:'flex', flexDirection:'row', marginTop: '5%',marginBottom:'2%', marginHorizontal:'5%'}}>
        <MedicineCabinetIcon/>
        <Text style={{paddingLeft:10,fontSize: 16, color: '#0c0c0c', fontWeight: 'bold'}}>Medicine Cabinet</Text>
        </View>

        {/* ------------------------- STORE CARD ------------------------- */}

        <ItemCard/>
        <ItemCard/>
        <ItemCard/>
        <ItemCard/>
        <ItemCard/>
        <ItemCard/>
        <ItemCard/>
        <ItemCard/>
        <View style={{paddingBottom:'1%'}}>
        </View>
        </ScrollView> 
{/*--------------------------------------------------------------------------- */
/* ----------------------------FOOTER DATA ----------------------------------- */
/*                 Item cart total, Number of items, View Cart button.         */
/* --------------------------------------------------------------------------  */}

      <TouchableOpacity activeOpacity={.8}  onPress={() => {props.navigation.navigate('Cart');}} style={{height:50,backgroundColor:'#48c2a5'}}>
      <View style={{display: 'flex', flexDirection:'row', marginHorizontal:'4%', justifyContent:'space-between', paddingVertical:12 }}>
      <Text style={{fontSize:16,color:'white'}}> 1 Item | ₹30.74</Text>
      <View style={{display: 'flex',flexDirection:'row'}}>
      <Text style={{fontSize:16, color:'white', marginRight:10}}> view cart</Text>
      <Icon name="shopping-cart" size={20} style={{color :'#fff', marginTop:2}}/>
      </View>
      </View>  
    </TouchableOpacity>

    </SafeAreaView>
    )
}

/* ----------------------------- Navigation Bar ----------------------------- */

Store.navigationOptions={
    headerShown:false
 };


const styles = StyleSheet.create({
    screen:{
        flex:1,
      backgroundColor:'#ffffff',       
    }
});

export default Store;


