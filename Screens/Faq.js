 /* -------------------------------------------------------------------------- */
 /*  AUTHOR: FAIZA A
 /* -------------------------------------------------------------------------- */
 /*  DATE: 23rd March, 2021.
 /* -------------------------------------------------------------------------- */
 /*  PAGE DESCRIPTION: 
 /*  The page allows the user to add location
 /* -------------------------------------------------------------------------- */
 import React from 'react';
 import {SafeAreaView, View, StyleSheet, Text, TextInput, TouchableWithoutFeedback, TouchableOpacity, Image} from 'react-native';
 import { ScrollView } from 'react-native-gesture-handler';
 import Icon from 'react-native-vector-icons/Feather';
 import FontAwesome from 'react-native-vector-icons/FontAwesome';

 
const Faq = props => {
  
     return (
        <SafeAreaView style={styles.screen}>
        <View style={{display: 'flex',backgroundColor: '#fff', flexDirection:'row',
        paddingTop:'10%', paddingHorizontal:'4%', borderBottomColor:'#ECECEC', borderBottomWidth:1, paddingBottom:'4%'}}>
        <TouchableOpacity onPress={() => {props.navigation.popToTop();}} style={{paddingTop:2}}>       
        <Icon name="arrow-left" size={22} style={{color :'#48C2A5'}}/></TouchableOpacity>
        <Text style={{fontSize: 18, color: '#000000', fontWeight: 'bold', paddingLeft:10}}>FAQ</Text>
        </View>
        <ScrollView style={{marginHorizontal:'4%', paddingTop:'2%'}}>
        <View style={{paddingVertical:'4%'}}>
        <Text style={{fontSize:16, fontWeight:'bold'}}>What is Kuiq?</Text>
        <Text style={{fontSize:14}}>Kuiq is a medicine delivery app. Kuiq is a medicine delivery app. Kuiq is a medicine delivery app.</Text>
        </View>

        <View style={{paddingVertical:'4%'}}>
        <Text style={{fontSize:16, fontWeight:'bold'}}>What is Kuiq?</Text>
        <Text style={{fontSize:14}}>Kuiq is a medicine delivery app. Kuiq is a medicine delivery app. Kuiq is a medicine delivery app.</Text>
        </View>

        <View style={{paddingVertical:'4%'}}>
        <Text style={{fontSize:16, fontWeight:'bold'}}>What is Kuiq?</Text>
        <Text style={{fontSize:14}}>Kuiq is a medicine delivery app. Kuiq is a medicine delivery app. Kuiq is a medicine delivery app.</Text>
        </View>

        <View style={{paddingVertical:'4%'}}>
        <Text style={{fontSize:16, fontWeight:'bold'}}>What is Kuiq?</Text>
        <Text style={{fontSize:14}}>Kuiq is a medicine delivery app. Kuiq is a medicine delivery app. Kuiq is a medicine delivery app.</Text>
        </View>

        <View style={{paddingVertical:'4%'}}>
        <Text style={{fontSize:16, fontWeight:'bold'}}>What is Kuiq?</Text>
        <Text style={{fontSize:14}}>Kuiq is a medicine delivery app. Kuiq is a medicine delivery app. Kuiq is a medicine delivery app.</Text>
        </View>

        <View style={{paddingVertical:'4%'}}>
        <Text style={{fontSize:16, fontWeight:'bold'}}>What is Kuiq?</Text>
        <Text style={{fontSize:14}}>Kuiq is a medicine delivery app. Kuiq is a medicine delivery app. Kuiq is a medicine delivery app.</Text>
        </View>
        </ScrollView>
        </SafeAreaView>
     )
 }
 
 /* ----------------------------- Navigation Bar ----------------------------- */
 
 Faq.navigationOptions={
     headerShown:false
  };
 
 
 const styles = StyleSheet.create({
     screen:{
         flex:1,
       backgroundColor:'#fff',   
     }
 });
 
 export default Faq;
 
 
 