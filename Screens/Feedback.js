 /* -------------------------------------------------------------------------- */
 /*  AUTHOR: FAIZA A
 /* -------------------------------------------------------------------------- */
 /*  DATE: 23rd March, 2021.
 /* -------------------------------------------------------------------------- */
 /*  PAGE DESCRIPTION: 
 /*  The page allows the user to add location
 /* -------------------------------------------------------------------------- */
 import React from 'react';
 import {SafeAreaView, View, StyleSheet, Text, TextInput, TouchableWithoutFeedback, TouchableOpacity, Image} from 'react-native';
 import { ScrollView } from 'react-native-gesture-handler';
 import Icon from 'react-native-vector-icons/Feather';
 import FontAwesome from 'react-native-vector-icons/FontAwesome';

 
const Feedback = props => {
  
     return (
        <SafeAreaView style={styles.screen}>
          <View style={{display: 'flex',backgroundColor: '#fff', flexDirection:'row',
        paddingTop:'10%', paddingHorizontal:'4%', borderBottomColor:'#ECECEC', borderBottomWidth:1, paddingBottom:'4%'}}>
        <TouchableOpacity onPress={() => {props.navigation.navigate('Profile');}} style={{paddingTop:2, paddingRight:10}}>       
        <Icon name="arrow-left" size={22} style={{color :'#48C2A5'}}/></TouchableOpacity>
        <Text style={{fontSize: 18, color: '#000000', fontWeight: 'bold', paddingLeft:5}}>Send Feeback</Text>
        </View> 
        <View style={{paddingHorizontal:'4%'}}> 
        <Text style={{fontSize:16, paddingTop:10, color:'#000'}}>
        Tell us what you love about the app, or what we could improve at.
        </Text>
        <TextInput style={{borderBottomColor:'#ECECEC', borderBottomWidth:1, paddingVertical:10, fontSize:16}} placeholder="Enter feedback"/>
        </View>
        </SafeAreaView>
     )
 }
 
 /* ----------------------------- Navigation Bar ----------------------------- */
 
 Feedback.navigationOptions={
     headerShown:false
  };
 
 
 const styles = StyleSheet.create({
     screen:{
         flex:1,
       backgroundColor:'#fff',   
     }
 });
 
 export default Feedback;
 
 
 