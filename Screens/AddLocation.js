 /* -------------------------------------------------------------------------- */
 /*  AUTHOR: FAIZA A
 /* -------------------------------------------------------------------------- */
 /*  DATE: 23rd March, 2021.
 /* -------------------------------------------------------------------------- */
 /*  PAGE DESCRIPTION: 
 /*  The page allows the user to add location
 /* -------------------------------------------------------------------------- */
 import React from 'react';
 import {SafeAreaView, View, StyleSheet, Text, TextInput, TouchableWithoutFeedback, TouchableOpacity, Image} from 'react-native';
 import { ScrollView } from 'react-native-gesture-handler';
 import Icon from 'react-native-vector-icons/Feather';
 import FontAwesome from 'react-native-vector-icons/FontAwesome';

 
const AddLocation = props => {
  
     return (
         <SafeAreaView style={styles.screen}>
         <View style={{paddingBottom:"2%"}}>
         {/* -----------------GOOGLE IMAGE--------------------------------------------*/
         /* -----------------WILL BE REPLACED WITH GOOGLE API LATER ON--------------------------- */}
         <Image source={require('../assets/map.jpg')} style={{width:'100%',height: '75%', resizeMode:'stretch', marginTop:'8%'}}/>
         <TouchableOpacity onPress={() => {props.navigation.navigate('Address');}} 
         style={{display: 'flex', flexDirection: 'row',marginHorizontal:'4%', position: 'absolute', marginTop:'10%'}}>
         <Icon name="arrow-left" size={26} style={{color :'#48C2A5'}}/>
         </TouchableOpacity>
         {/* ----------------------------------------------------------------------------------*/
         /* -----------------CHANGE LOCATION-------------------------------------------------- */}
         
         <View style={{marginTop:'1%',flexDirection:'row', paddingVertical:'2%', borderBottomColor:'#f3f3f3', 
          borderBottomWidth:1 }}>
          <Text style={{fontSize:18, color:'#89898B', fontWeight:'bold', paddingLeft:'4%'}}>Select Delivery Location</Text>
         </View>
         {/* ----------------------------------------------------------------------------------*/
         /* ------------------------------------------------------------------- */}
         <Text style={{paddingTop:'2%',fontSize: 10, color:'#a1a1a1',fontWeight:'bold', 
          marginHorizontal:'4%', paddingBottom:'1%'}}>YOUR LOCATION</Text>
        
         <View style={{display: 'flex',flexDirection:'row', marginTop:'1%', justifyContent:'space-between', marginHorizontal:'4%'}}>
         <View style={{display: 'flex',flexDirection:'row'}}>
         <TouchableOpacity>
         <Icon name="check-circle" size={15} style={{color :'#48c2a5',paddingTop:'0.7%'}}/>
         </TouchableOpacity> 
         <Text style={{fontSize:14, color:'#89898B', fontWeight:'bold', 
         paddingLeft:'2%'}}>13, Mullen Street, Ballygunge Circular...</Text>
         </View>
         <TouchableOpacity onPress={() => {props.navigation.navigate('Address');}}>
         <Text style={{fontSize:14, color:'#f6665b'}}>Change</Text>
         </TouchableOpacity>
         </View>
         {/* ----------------------------------------------------------------------------------*/
         /* ------------------------------------BUTTON---------------------------------------------- */}
         <TouchableOpacity   onPress={() => {props.navigation.navigate('AddressDetails');}} style={{height:50,backgroundColor:'#48c2a5',
         borderRadius:50,elevation:5,marginTop:'3%', marginHorizontal:'4%', justifyContent:'center',alignItems:'center'}}>
         <Text style={{fontSize:16,color:'white',fontWeight:'600'}}>Confirm Location</Text>
         </TouchableOpacity>
         </View>
         
     </SafeAreaView>
     )
 }
 
 /* ----------------------------- Navigation Bar ----------------------------- */
 
 AddLocation.navigationOptions={
     headerShown:false
  };
 
 
 const styles = StyleSheet.create({
     screen:{
         flex:1,
       backgroundColor:'#ffffff',       
     }
 });
 
 export default AddLocation;
 
 
 