 /* -------------------------------------------------------------------------- */
 /*  AUTHOR: FAIZA A
 /* -------------------------------------------------------------------------- */
 /*  DATE: 23rd March, 2021.
 /* -------------------------------------------------------------------------- */
 /*  PAGE DESCRIPTION: 
 /*  The page allows the user to add location
 /* -------------------------------------------------------------------------- */
 import React from 'react';
 import {SafeAreaView, View, StyleSheet, Text, TextInput, TouchableWithoutFeedback, TouchableOpacity, Image} from 'react-native';
 import { ScrollView } from 'react-native-gesture-handler';
 import Icon from 'react-native-vector-icons/Feather';
 import FontAwesome from 'react-native-vector-icons/FontAwesome';

 
const Coupon = props => {
  
     return (
        <SafeAreaView style={styles.screen}>
        <View style={{display: 'flex',backgroundColor: '#fff', flexDirection:'row',
        paddingTop:'10%', paddingHorizontal:'4%', borderBottomColor:'#ECECEC', borderBottomWidth:1, paddingBottom:'4%'}}>
        <TouchableOpacity onPress={() => {props.navigation.navigate('Store');}} style={{paddingTop:2}}>       
        <Icon name="arrow-left" size={22} style={{color :'#48C2A5'}}/></TouchableOpacity>
        <Text style={{fontSize: 18, color: '#000000', fontWeight: 'bold', paddingLeft:10}}>Available Offers</Text>
        </View>
         {/* Card Design  */}
        <View style={{marginBottom:10, paddingHorizontal:'4%', paddingVertical:'4%',backgroundColor:'#fff'}}>
        <View style={{borderBottomColor:'#ECECEC', borderBottomWidth:1, paddingBottom:'4%'}}>
        <Text style={{fontSize:16, fontWeight:'bold'}}>Get 20% off upto ₹50</Text>
        <Text style={{fontSize:12, color:'#909090'}}>Valid only on medicines</Text>
        </View>
        <View style={{flexDirection:'row', justifyContent:'space-between', paddingTop:10}}>
        <View style={{borderColor:'#A7B4E2', borderWidth:1, paddingHorizontal:10, paddingVertical:5, backgroundColor:'#e4f3fe'}}><Text>CODELMN</Text></View>
        <Text style={{fontSize:14, color:'#f6665b'}}>Apply</Text>
        </View>
        </View>
        {/* card design end */}
        {/* Card Design  */}
        <View style={{marginBottom:10, paddingHorizontal:'4%', paddingVertical:'4%',backgroundColor:'#fff'}}>
        <View style={{borderBottomColor:'#ECECEC', borderBottomWidth:1, paddingBottom:'4%'}}>
        <Text style={{fontSize:16, fontWeight:'bold'}}>Get 20% off upto ₹50</Text>
        <Text style={{fontSize:12, color:'#909090'}}>Valid only on medicines</Text>
        </View>
        <View style={{flexDirection:'row', justifyContent:'space-between', paddingTop:10}}>
        <View style={{borderColor:'#A7B4E2', borderWidth:1, paddingHorizontal:10, paddingVertical:5, backgroundColor:'#e4f3fe'}}><Text>CODELMN</Text></View>
        <Text style={{fontSize:14, color:'#f6665b'}}>Apply</Text>
        </View>
        </View>
        {/* card design end */}
        {/* Card Design  */}
        <View style={{marginBottom:10, paddingHorizontal:'4%', paddingVertical:'4%',backgroundColor:'#fff'}}>
        <View style={{borderBottomColor:'#ECECEC', borderBottomWidth:1, paddingBottom:'4%'}}>
        <Text style={{fontSize:16, fontWeight:'bold'}}>Get 20% off upto ₹50</Text>
        <Text style={{fontSize:12, color:'#909090'}}>Valid only on medicines</Text>
        </View>
        <View style={{flexDirection:'row', justifyContent:'space-between', paddingTop:10}}>
        <View style={{borderColor:'#A7B4E2', borderWidth:1, paddingHorizontal:10, paddingVertical:5, backgroundColor:'#e4f3fe'}}><Text>CODELMN</Text></View>
        <Text style={{fontSize:14, color:'#f6665b'}}>Apply</Text>
        </View>
        </View>
        {/* card design end */}
        {/* Card Design  */}
        <View style={{marginBottom:10, paddingHorizontal:'4%', paddingVertical:'4%',backgroundColor:'#fff'}}>
        <View style={{borderBottomColor:'#ECECEC', borderBottomWidth:1, paddingBottom:'4%'}}>
        <Text style={{fontSize:16, fontWeight:'bold'}}>Get 20% off upto ₹50</Text>
        <Text style={{fontSize:12, color:'#909090'}}>Valid only on medicines</Text>
        </View>
        <View style={{flexDirection:'row', justifyContent:'space-between', paddingTop:10}}>
        <View style={{borderColor:'#A7B4E2', borderWidth:1, paddingHorizontal:10, paddingVertical:5, backgroundColor:'#e4f3fe'}}><Text>CODELMN</Text></View>
        <Text style={{fontSize:14, color:'#f6665b'}}>Apply</Text>
        </View>
        </View>
        {/* card design end */}
        </SafeAreaView>
     )
 }
 
 /* ----------------------------- Navigation Bar ----------------------------- */
 
 Coupon.navigationOptions={
     headerShown:false
  };
 
 
 const styles = StyleSheet.create({
     screen:{
         flex:1,
       backgroundColor:'#f5f5f5',   
     }
 });
 
 export default Coupon;
 
 
 