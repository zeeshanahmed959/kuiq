/* -------------------------------------------------------------------------- */
 /*  AUTHOR: ZEESHAN A
 /* -------------------------------------------------------------------------- */
 /*  DATE: 15th March, 2021.
 /* -------------------------------------------------------------------------- */
 /*  PAGE DESCRIPTION: 
 /*  The page displays Payment Method.
 /*  This page should open when user selects payment method.
 /* -------------------------------------------------------------------------- */

 import React, {useState} from 'react';
 import {SafeAreaView, View, StyleSheet, Text, TextInput, Dimensions, TouchableWithoutFeedback, TouchableOpacity, Image, addons} from 'react-native';
 import { ScrollView } from 'react-native-gesture-handler';
 import Icon from 'react-native-vector-icons/Feather';
 import FontAwesome from 'react-native-vector-icons/FontAwesome';
 import UPIIcon from '../assets/UPIIcon';
 
const PaymentMethod = props => {
    const [CardForm, setCardForm] = useState(0);
    const [UPIForm, setUPIForm] = useState(0);

    const toggleCard = () =>{
        CardForm ? setCardForm(0) : setCardForm(1);
    }

    const toggleUPI = () =>{
        UPIForm ? setUPIForm(0) : setUPIForm(1);
    }

    return (
        <SafeAreaView style={styles.screen}>

        {/* HEADER START */}
        <View style={{paddingBottom:15,display:'flex', flexDirection:'row', paddingTop:'10%',
         paddingHorizontal:'4%',borderBottomColor:'#f3f3f3', borderBottomWidth:1}}>
        <TouchableOpacity onPress={() => {props.navigation.popToTop()}}>
        <Icon name="arrow-left" size={22} style={{color :'#48C2A5', marginTop:2}}/>
        </TouchableOpacity>
        <Text style={{fontSize:18, fontWeight:'bold', marginLeft:10}}>Payment Method</Text>
        </View>
        {/*  HEADER END */}

        <ScrollView>
        <View style={{paddingHorizontal:'4%'}}>

        {/*  CARD SECTION */}
        <View style={{paddingVertical:'5%'}}>
        <Text style={{fontSize:18, color:'#000'}}>Cards</Text>
        <View style={{display:'flex', flexDirection:'row', paddingTop: '5%', justifyContent:'space-between'}}>
        <View style={{display:'flex', flexDirection:'row'}}>
        <FontAwesome name='credit-card' size={22} style={{paddingTop:8}}/>
        <View style={{paddingLeft:10}}>
        <Text style={{fontSize:14}}>Faiza Ahmad</Text>
        <Text style={{fontSize:12, color:'#a1a1a1'}}>4783XXXXXXX8762</Text>
        </View>
        </View>
        <View style={{paddingTop:3}}><Icon name='x-circle' size={16} color='#FF6F6F'/></View>
        </View>
        <TouchableOpacity onPress={toggleCard}>
        <View style={{flexDirection:'row',paddingTop:'5%'}}>
        <FontAwesome size={14} style={{paddingTop:5,color:'#000',paddingRight:10}} name='plus-square'/>
        <Text>Add Credit, Debit & ATM Cards</Text>
        </View>
        </TouchableOpacity>
        {CardForm ? (
        <View style={{paddingTop:'5%'}}>
        <TextInput style={{height:50,width:'100%',backgroundColor:'white',borderRadius:25,borderWidth:1,borderColor:'#f3f3f3',paddingLeft:20, shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
         },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 2.2}} placeholder='Name On Card'/>
        <TextInput style={{height:50,width:'100%',backgroundColor:'white',borderRadius:25,borderWidth:1,borderColor:'#f3f3f3',paddingLeft:20, marginTop:20, shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
         },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 2.2}} placeholder='Card Number'/>
        <View style={{flexDirection:'row', justifyContent:'space-between'}}>
        <TextInput style={{height:50,width:'55%',backgroundColor:'white',borderRadius:25,borderWidth:1,borderColor:'#f3f3f3', marginTop:20,paddingLeft:20, shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
         },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 2.2}} placeholder='Expiry Date (MM/YY)'/>
        <TextInput style={{height:50,width:'35%',backgroundColor:'white',borderRadius:25,borderWidth:1,borderColor:'#f3f3f3', marginTop:20,paddingLeft:20, shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
         },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 2.2}} placeholder='CVV'/>
        </View>
        <TouchableOpacity  style={{width:'100%', height:50,backgroundColor:'#48c2a5',borderRadius:25,elevation:5, marginTop:20, justifyContent:'center',alignItems:'center',marginBottom:4}}>
        <Text style={{fontSize:16,color:'white',fontWeight:'600'}}>Save Card</Text>
        </TouchableOpacity>
        </View>
        ) : null }
         {/*  END OF CARD SECTION */}
        
        </View>

        {/*  UPI SECTION */}
        <View style={{paddingVertical:'5%'}}>
        <Text style={{fontSize:18, color:'#000'}}>UPI</Text>
        <View style={{display:'flex', flexDirection:'row', paddingTop: '5%', justifyContent:'space-between'}}>
        <View style={{display:'flex', flexDirection:'row'}}>
        <UPIIcon/>
        <Text style={{fontSize:14, paddingLeft:10}}>zeeshanhamed959@okhdfcbank</Text>
        </View>
        <View style={{paddingTop:3}}><Icon name='x-circle' size={16} color='#FF6F6F'/></View>
        </View>
        <TouchableOpacity onPress={toggleUPI}>
        <View style={{flexDirection:'row',paddingTop:'5%'}}>
        <FontAwesome size={14} style={{paddingTop:5,color:'#000',paddingRight:10}} name='plus-square'/>
        <Text>Add UPI</Text>
        </View>
        </TouchableOpacity>
        {UPIForm ? (
        <View style={{paddingTop:'5%'}}>
        <TextInput style={{height:50,width:'100%',backgroundColor:'white',borderRadius:25,borderWidth:1,borderColor:'#f3f3f3',paddingLeft:20, shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
         },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 2.2}} placeholder='Enter UPI ID'/>
        <TouchableOpacity  style={{width:'100%', height:50,backgroundColor:'#48c2a5',borderRadius:25,elevation:5, marginTop:20, justifyContent:'center',alignItems:'center',marginBottom:4}}>
        <Text style={{fontSize:16,color:'white',fontWeight:'600'}}>Save UPI</Text>
        </TouchableOpacity>
        </View>
        ) : null}
        </View>
        {/*  END OF UPI SECTION */}

        {/*  COD SECTION */}
        <View style={{paddingVertical:'5%'}}>
        <Text style={{fontSize:18, color:'#000'}}>Cash On Delivery</Text>
        <View style={{flexDirection:'row',paddingTop:'5%'}}>
        <FontAwesome size={14} style={{paddingTop:4,color:'#000',paddingRight:10}} name='money'/>
        <Text>Select Cash On Delivery</Text>
        </View>
        </View>
        {/*  END OF COD SECTION */}

        </View>
        </ScrollView>
        </SafeAreaView>
    )
}

PaymentMethod.navigationOptions={
    headerShown:false
 };

const styles = StyleSheet.create({
    screen:{
        flex:1,
      backgroundColor:'#ffffff', 
    }
});

export default PaymentMethod;