 /* -------------------------------------------------------------------------- */
 /*  AUTHOR: FAIZA A
 /* -------------------------------------------------------------------------- */
 /*  DATE: 23rd March, 2021.
 /* -------------------------------------------------------------------------- */
 /*  PAGE DESCRIPTION: 
 /*  The page allows the user to add location
 /* -------------------------------------------------------------------------- */
 import React from 'react';
 import {SafeAreaView, Animated, View, StyleSheet, Text, Dimensions, TextInput, TouchableWithoutFeedback, TouchableOpacity, Image} from 'react-native';
 import { PinchGestureHandler, State, ScrollView } from 'react-native-gesture-handler';
 import Icon from 'react-native-vector-icons/Feather';
 import PrescriptionIcon from '../assets/PrescriptionIcon';
 import FontAwesome from 'react-native-vector-icons/FontAwesome';

const PrescriptionView = props => {
    return (
        <SafeAreaView style={styles.screen}>
        <View style={{flexDirection:'column', justifyContent:'space-between', height:'100%', marginHorizontal:'4%'}}>
        <View style={{flexDirection:'row', justifyContent:'space-between'}}>
        <TouchableOpacity onPress={() => {props.navigation.popToTop();}} style={{paddingTop:40}}>       
        <Icon name="x" size={22} style={{color :'#48C2A5'}}/>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {props.navigation.popToTop();}} style={{paddingTop:40}}>       
        <Icon name="trash-2" size={18} style={{color :'#48C2A5'}}/>
        </TouchableOpacity>
        </View>
        <View style={{flexDirection:'row',marginHorizontal:'4%'}}>
        <Image 
        source={require('../assets/prescription.png')} 
        style={{resizeMode:'contain', aspectRatio:1, flex:1}}/>
        </View>
        <TouchableOpacity   onPress={() => {}} style={{height:50,backgroundColor:'#ADADAD',
         borderRadius:5,elevation:5,marginTop:'3%', width:200, marginBottom:20, flexDirection:'row', justifyContent:'center',alignItems:'center'}}>
         <Icon name="check-square" size={20} style={{color :'#fff', paddingRight: 10}}/>
         <Text style={{fontSize:16,color:'white',fontWeight:'600'}}>Select Prescription</Text>
         </TouchableOpacity>
         </View>
        </SafeAreaView>
    )
 }
 
 /* ----------------------------- Navigation Bar ----------------------------- */
 
 PrescriptionView.navigationOptions={
     headerShown:false
  };
 
 
 const styles = StyleSheet.create({
     screen:{
       backgroundColor:'rgba(0,0,0,0.8)',   
     }
 });
 
 export default PrescriptionView;
 
 
 