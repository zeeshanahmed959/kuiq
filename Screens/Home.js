 /* -------------------------------------------------------------------------- */
 /*  AUTHOR: ZEESHAN A
 /* -------------------------------------------------------------------------- */
 /*  DATE: 5th March, 2021.
 /* -------------------------------------------------------------------------- */
 /*  PAGE DESCRIPTION: 
 /*  The page displays the pharmacies near you list.
 /*  The user select the pharmacy to buy medicines from here.
 /* -------------------------------------------------------------------------- */
import React from 'react';
import {SafeAreaView,View, StyleSheet, Text, TextInput, TouchableWithoutFeedback, TouchableOpacity, Image} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import LocationIcon from '../assets/LocationIcon';
import StoreIcon from '../assets/StoreIcon';
import StoreCard from '../components/StoreCard';

const Home = props => {
    return(
        <SafeAreaView style={styles.screen}>

        {/* -----------------HEADER HOME PAGE--------------------------------------------*/
         /* -----------------CONTAINS LOCATION AND SEARCH BAR--------------------------- */}

         <View style={{flexDirection:'column', borderBottomColor:'#ECECEC', borderBottomWidth:1, paddingBottom:8}}>
        
         {/* ----------------LOCATION-------------------------------------------------- */}

         <View style={{flexDirection: 'row'}}>
         <TouchableOpacity onPress={() => {props.navigation.navigate('AddLocation');}} style={{marginTop: '11%', marginLeft: '4%'}}>
         <LocationIcon/>
         </TouchableOpacity>
         <View style={{flexDirection:'column', marginTop: '10%', marginLeft: '2%'}}>
         <Text style={{fontSize: 12, color: '#89898B'}}>Deliver to</Text>
         <TouchableOpacity onPress={() => {props.navigation.navigate('AddLocation');}}>
         <Text style={{fontSize: 13, color: '#50BEA4', fontWeight: 'bold'}}>Ballygunge Area, Kolkata</Text>
         </TouchableOpacity>
         </View>
         </View>

         {/* ----------------SEARCH BAR-------------------------------------------------- */}
         {/* <TouchableWithoutFeedback onPress={() => {props.navigation.navigate('Search');}}> 
         <View style={{ flexDirection: 'row', backgroundColor: '#fff', 
         borderWidth: 0.5, borderColor: '#ececec', justifyContent: 'center', alignItems: 'center',
         height: 50, borderRadius: 25, margin: 10, shadowColor: "#000",
         shadowOffset: {
                width: 0,
                height: 2,
            },
         shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 2,}}>
         <Icon name="search" size={21} style={{color :'#000000',padding: 10, paddingRight:5,
            margin: 4, alignItems: 'center',}}/>
         <Text style={{flex: 1, color:'#b4b4b4'}}>Search for medicines...</Text>
         </View>
         </TouchableWithoutFeedback> */}
      
        {/* ----------END OF SEARCH BAR------------------------------------------------- */}
         </View>
        {/* ----------END OF HEADER HOME PAGE---------------------------------------------*/
          /*-------------CONTAINS LOCATION AND SEARCH BAR--------------------------------- */}
        {/* ----------SCROLL VIEW-------------------------------------------------------- */}

         <ScrollView style={{paddingBottom: '10%'}}>
        {/* --------------------------------BANNER---------------------------------- */}
         <View style={{justifyContent: 'center', alignItems: 'center',marginTop:'5%',
          flexDirection:'row', justifyContent:'space-around', marginHorizontal:'4%', borderTopColor:'#f3f3f3', 
          borderTopWidth:1 ,shadowColor: "#000",
          shadowOffset: {
             width: 3,
             height: 5,
          },
          shadowOpacity: 2,
          shadowRadius: 2,
          elevation: 6, borderRadius: 20}}> 
         <Image source={require('../assets/banner1.png')} style={{width:'100%', resizeMode:'stretch'}}/>
         </View>
        {/* --------------------------------PHARMACIES NEAR YOU---------------------------------- */}
         <View style={{flexDirection:'row', marginTop: '5%', marginBottom: '2%'}}>
         <View style={{marginHorizontal:'4%'}}>
         <StoreIcon/>
         </View>
         <Text style={{fontSize:16, color:'#0c0c0c', fontWeight:'bold', paddingLeft:10}}>Pharmacies Near You</Text>
         </View>
        {/* --------------------------------STORE CARDS---------------------------------- */}
         <TouchableWithoutFeedback onPress={() => {props.navigation.navigate('Store');}}>
         <View>
         <StoreCard/>
         </View>
         </TouchableWithoutFeedback>
         <StoreCard/>
         <StoreCard/>
         <StoreCard/>
         <StoreCard/>
         <StoreCard/>
         <View style={{paddingBottom:10}}>
         </View>
         </ScrollView>
         <View  style={{backgroundColor:'#F6F8FA',  paddingHorizontal:'4%', flexDirection:'row', justifyContent:'space-between', paddingVertical:10}}>
         <View>
         <Text style={{fontWeight:'bold', fontSize:12}}>Random Pharmacy</Text>
         <Text style={{color:'#a1a1a1', fontSize:12}}>You have items saved in your cart</Text>
         </View>
         <Text style={{fontSize:12, color:'#50BEA4'}}>View</Text>
         </View>
        {/* ----------END OF SCROLL VIEW------------------- */}
        </SafeAreaView>
    )
}
{/* -------------------------NAVIGATION BAR SETTINGS------------------------------------------------- */} 
Home.navigationOptions={
    headerShown:false
 };

const styles = StyleSheet.create({
    screen:{
        flex:1,
      backgroundColor:'#ffffff',       
    },
    input:{
        height:50,
        width:53,
        backgroundColor:'white',
        borderRadius:25,
        borderWidth:1,
        borderColor:'#f3f3f3',
        textAlign:'center',
        fontSize:16,
        fontWeight:'700'
    }

});
export default Home;