/* -------------------------------------------------------------------------- */
 /*  AUTHOR: FAIZA A
 /* -------------------------------------------------------------------------- */
 /*  DATE: 24th March, 2021.
 /* -------------------------------------------------------------------------- */
 /*  PAGE DESCRIPTION: 
 /*  The page displays About section
 /* -------------------------------------------------------------------------- */
 import React from 'react';
 import {SafeAreaView, View, StyleSheet, Text, TextInput, Dimensions, TouchableOpacity, Image} from 'react-native';
 import Icon from 'react-native-vector-icons/Feather';
 import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { back } from 'react-native/Libraries/Animated/src/Easing';
 
 const About = props => {
     return (
         <SafeAreaView style={styles.screen}>
           <View style={{backgroundColor:'#fff'}}>
          {/* -----------------BACK BUTTON--------------------------------------------*/
         /* ------------------------------------------------------------------------ */}
         <View style={{display: 'flex',backgroundColor: '#fff', flexDirection:'row',
        paddingTop:'10%', paddingHorizontal:'4%', borderBottomColor:'#ECECEC', borderBottomWidth:1, paddingBottom:'4%'}}>
        <TouchableOpacity onPress={() => {props.navigation.popToTop();}} style={{paddingTop:2, paddingRight:10}}>       
        <Icon name="arrow-left" size={22} style={{color :'#48C2A5'}}/></TouchableOpacity>
        <Text style={{fontSize: 18, color: '#000000', fontWeight: 'bold', paddingLeft:5}}>About</Text>
        </View>
     {/*--------------------------------------------------------------------------- */
      /*                             TERMS OF SERVICE  & Privacy Policy              */
      /* --------------------------------------------------------------------------  */}
          <TouchableOpacity onPress={() => {props.navigation.navigate('TermsOfService');}} style={{display: 'flex', flexDirection:'row',marginTop:10, marginbottom:10,
          paddingBottom: 10, justifyContent:'space-between', paddingRight:'1%',borderColor:'#f3f3f3', 
          borderBottomWidth:1,paddingTop:10}}>
          <Text style={{marginLeft:20, color:'#000',fontSize: 16, padding:8,}}>Terms of Service</Text>
          <Icon name ='chevron-right' size={18} style={{color :'#000', padding:8,}}/>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => {props.navigation.navigate('TermsOfService');}} style={{display: 'flex', flexDirection:'row',marginTop:10, marginbottom:10,
          paddingBottom: 10, justifyContent:'space-between', paddingRight:'1%',borderColor:'#f3f3f3', 
          borderBottomWidth:1,paddingTop:10}}>
          <Text style={{marginLeft:20, color:'#000',fontSize: 16, padding:8,}}>Privacy Policy</Text>
          <Icon name ='chevron-right' size={18} style={{color :'#000', padding:8,}}/>
          </TouchableOpacity>
          
     { /*-----------------------------APP VERSION------------------------------------ */
     /* ----------------------------------------------------------------------------  */}
        
          <TouchableOpacity style={{display: 'flex', flexDirection:'row', marginbottom:10,
          paddingVertical: 10, justifyContent:'space-between', paddingRight:'1%',borderColor:'#f3f3f3', 
          borderBottomWidth:1, paddingLeft:'2%'}}>
          <View style={{display: 'flex', flexDirection:'column'}}>
          <Text style={{marginLeft:20, color:'#000',fontSize: 14, fontWeight:'bold'}}>App Version</Text>
          <Text style={{marginLeft:20, color:'#000',fontSize: 14}}>v15.5.3 Live</Text>
          </View>
          </TouchableOpacity>
     
    { /*----------------------------------------------------------------------------- */}
          </View>
     </SafeAreaView>
     )
 }
 
 {/*--------------------------NAV BAR footer------------------------------------------ */}
 About.navigationOptions={
     headerShown:false
  };
 
 const styles = StyleSheet.create({
     screen:{
         flex:1,
       backgroundColor:'#fff',       
     }
 });
 
 export default About;
 