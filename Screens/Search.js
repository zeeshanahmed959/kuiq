 /* -------------------------------------------------------------------------- */
 /*  AUTHOR: FAIZA A
 /* -------------------------------------------------------------------------- */
 /*  DATE: 18th March, 2021.
 /* -------------------------------------------------------------------------- */
 /*  PAGE DESCRIPTION: 
 /*  The page displays the medicine suggesstion list for the search Bar
 /* -------------------------------------------------------------------------- */
 import React from 'react';
 import {SafeAreaView, ScrollView, View, StyleSheet, Text, TextInput, Dimensions, TouchableWithoutFeedback, TouchableOpacity, Image} from 'react-native';
 import Icon from 'react-native-vector-icons/Feather';
 import TabletsIcon from '../assets/TabletsIcon';
 import StoreCard from '../components/StoreCard';
 import SearchListCard from '../components/SearchListCard';

 const Search = props => {
     return (
         <SafeAreaView style={styles.screen}>
         {/* <View style={{paddingBottom:20}}>
         <TouchableOpacity onPress={() => {props.navigation.navigate('Home');}} style={{display: 'flex', flexDirection: 'row', marginTop: '10%', marginLeft: 10}}>
         <Icon name="arrow-left" size={22} style={{color :'#48C2A5'}}/>
         </TouchableOpacity>
         </View> */}
 {/*--------------------------------------------------------- */
 /* ----------------------------SEARCH BAR ------------------------------------ */
 /* --------------------------------------------------------------------------  */}
         <TouchableWithoutFeedback onPress={() => {props.navigation.navigate({routeName: 'Search2'});}}>
         <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center',
         backgroundColor: '#fff', marginTop:40, borderWidth: 0.5, borderColor: '#ececec', 
         height: 50, borderRadius: 25, margin: 10, shadowColor: "#000",
         shadowOffset: {
                 width: 0,
                 height: 3,
             },
         shadowOpacity: 0.25, shadowRadius: 4.4, elevation: 5,}}>
         <Icon name="search" size={21} style={{color :'#000000',padding: 10, paddingRight:5,
             margin: 4, alignItems: 'center',}}/>
           <TextInput
             style={{flex: 1}}
             placeholder="Search for medicines..."
           />
         </View>
         </TouchableWithoutFeedback>
         
         {/* --------------------------- End of search Bar -------------------------- */}
         <View style={{display: 'flex', flexDirection:'column'}}>
         <ScrollView style={{paddingBottom: '50%'}}>
          {/*--------------------------------------------------------------------------- */
           /*                             SEARCH SUGGESTION CARD               */
           /* --------------------------------------------------------------------------  */}
           <SearchListCard/>
           <SearchListCard/>
           <SearchListCard/>
           <SearchListCard/>
          {/*--------------------------------------------------------------------------- */
           /*                             SEARCH SUGGESTION RESULTS               */
           /* --------------------------------------------------------------------------  */}
         
           <TouchableWithoutFeedback onPress={() => {props.navigation.navigate('Store');}}>
           <View>
           <StoreCard/>
           </View>
           </TouchableWithoutFeedback>
           <StoreCard/>
           <StoreCard/>
           <StoreCard/>
           <StoreCard/>
           <StoreCard/>
           <View style={{paddingBottom:'25%'}}>
           </View>
         </ScrollView>
 
         {/* ----------END OF SCROLL VIEW------------------- */}
         </View>
     </SafeAreaView>
     )
 }
 
 /* ----------------------------- Navigation Bar ----------------------------- */
 
 Search.navigationOptions={
     headerShown:false
  };
 
 const styles = StyleSheet.create({
     screen:{
         flex:1,
       backgroundColor:'#ffffff',       
     }
 });
 
 export default Search;
 
 
 