/* -------------------------------------------------------------------------- */
 /*  AUTHOR: FAIZA A
 /* -------------------------------------------------------------------------- */
 /*  DATE: 15th March, 2021.
 /* -------------------------------------------------------------------------- */
 /*  PAGE DESCRIPTION: 
 /*  The page displays the Profile of the User. 
 /*  Can be navigated from the Navigation Bar
 /* -------------------------------------------------------------------------- */
import React, {useState} from 'react';
import {SafeAreaView, View, StyleSheet, Text, TextInput, Dimensions, TouchableOpacity, Image, Alert, Modal} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

const Profile = props => {
    const [modalVisible, setModalVisible] = useState(false);
    return (
        <SafeAreaView style={styles.screen}>
{/*--------------------------------------------------------------------------- */
/* ----------------------------HEADER DATA START------------------------------ */
/*                       PROFILE HEADER DATA                                   */
/* --------------------------------------------------------------------------  */}
         <View style={{paddingTop:35, paddingBottom: 10,display:'flex', flexDirection:'row',justifyContent:'space-between', borderBottomColor:'#f3f3f3', borderBottomWidth:1,backgroundColor: '#fff', paddingHorizontal:'4%'}}>
         
         <View style={{display: 'flex', flexDirection:'row'}}>
         {/* <View style={{display: 'flex', flexDirection:'column', marginTop:'3%'}}>
         <FontAwesome name="user-circle" size={50} style={{color :'#707070', justifyContent:'flex-end',}}/>
         </View> */}

         <View style={{display: 'flex', flexDirection:'column', justifyContent: 'flex-start', paddingLeft:'5%'}}>
         <Text style={{fontSize: 18, color: '#000000', fontWeight: 'bold',paddingTop:5}}>Faiza Ahmad</Text>
         {/* <Text style={{color:'#000',fontSize: 14}}>faiza.ahmad@kuiq.com</Text> */}
         <Text style={{paddingTop:3,fontSize: 12, color:'#707070', marginBottom:5}}>9831123468</Text>
         </View>
         </View>
         <TouchableOpacity style={{display: 'flex', flexDirection:'row', paddingTop:"1%",}}>
         <Icon name="edit" size={15} style={{color :'#50BEA4', justifyContent:'flex-end', paddingTop:3}}/>
         <Text style={{fontSize:14, color:'#50BEA4', justifyContent:'flex-end'}}> Edit</Text>
         </TouchableOpacity>
         </View>
{/*--------------------------------------------------------------------------- */
/*                             PROFILE CARD - MANAGE PAYMENTS                  */
/* --------------------------------------------------------------------------  */}
         <View style={{paddingHorizontal:'4%', marginTop:5, paddingVertical:'2%',backgroundColor:'#fff'}}>
         <TouchableOpacity onPress={() => {props.navigation.navigate('PaymentMethod');}} style={{display: 'flex', flexDirection:'column', }}>
         <View style={{display: 'flex', flexDirection:'row',marginTop:10, marginbottom:10, paddingBottom: 10, justifyContent:'space-between'}}>
         <View style={{display: 'flex', flexDirection:'row'}}>
         <Icon name='credit-card' size={15} style={{color :'#000', backgroundColor:'#EBEEF2', padding:8, borderRadius:50}}/>
         <Text style={{marginLeft:20, color:'#000',fontSize: 16}}>Manage Payments</Text>
         </View>
         </View>
         </TouchableOpacity> 
{ /*-----------------------------PROFILE CARD -SAVED ADDRESSES-------------------------------- */
/* --------------------------------------------------------------------------  */}
         <TouchableOpacity onPress={() => {props.navigation.navigate('Address');}} style={{display: 'flex', flexDirection:'column', }}>
         <View style={{display: 'flex', flexDirection:'row',marginTop:10, marginbottom:10,paddingBottom: 10, justifyContent:'space-between'}}>
         <View style={{display: 'flex', flexDirection:'row'}}>
         <Icon name ='map' size={15} style={{color :'#000', backgroundColor:'#EBEEF2', padding:8, borderRadius:50}}/>
         <Text style={{marginLeft:20, color:'#000',fontSize: 16}}>Saved Address</Text>
         </View>
         </View>
         </TouchableOpacity>  
{ /*-----------------------------PROFILE CARD - SUPPORT-------------------------------- */
/* --------------------------------------------------------------------------  */}
         <TouchableOpacity style={{display: 'flex', flexDirection:'column', }}>
         <View style={{display: 'flex', flexDirection:'row',marginTop:10, marginbottom:10,paddingBottom: 10, justifyContent:'space-between'}}>
         <View style={{display: 'flex', flexDirection:'row'}}>
         <Icon name='share-2' size={15} style={{color :'#000', backgroundColor:'#EBEEF2', padding:8, borderRadius:50}}/>
         <Text style={{marginLeft:20, color:'#000',fontSize: 16}}>Support</Text>
         </View>
         </View>
         </TouchableOpacity> 
{ /*-----------------------------PROFILE CARD- FAQs------------------------------- */
/* --------------------------------------------------------------------------  */}
         <TouchableOpacity onPress={() => {props.navigation.navigate('Faq');}} style={{display: 'flex', flexDirection:'column', }}>
         <View style={{display: 'flex', flexDirection:'row',marginTop:10, paddingBottom: 10, justifyContent:'space-between'}}>
         <View style={{display: 'flex', flexDirection:'row'}}>
         <Icon name='help-circle' size={15} style={{color :'#000', backgroundColor:'#EBEEF2', padding:8, borderRadius:50}}/>
         <Text style={{marginLeft:20, color:'#000',fontSize: 16}}>FAQs</Text>
         </View>
         </View>
         </TouchableOpacity>
{ /*-----------------------------PROFILE CARD- ABOUT------------------------------- */
/* --------------------------------------------------------------------------  */}
         <TouchableOpacity onPress={() => {props.navigation.navigate('About');}} style={{display: 'flex', flexDirection:'column', }}>
         <View style={{display: 'flex', flexDirection:'row',marginTop:10, marginbottom:10, paddingBottom: 10, justifyContent:'space-between'}}>
         <View style={{display: 'flex', flexDirection:'row'}}>
         <Icon name='info' size={15} style={{color :'#000', backgroundColor:'#EBEEF2', padding:8, borderRadius:50}}/>
         <Text style={{marginLeft:20, color:'#000',fontSize: 16}}>About</Text>
         </View>
         </View>
         </TouchableOpacity>
         </View>
{ /*-----------------------------PROFILE CARD ENDS-------------------------------- */
/* --------------------------------------------------------------------------  */}
         <View style={{display: 'flex', flexDirection:'column',paddingVertical:'5%', marginTop:5,
         backgroundColor:'#fff', paddingTop:15, paddingHorizontal:'4%' }}>
         <TouchableOpacity onPress={() => {props.navigation.navigate('Feedback');}}>
         <Text style={{color:'#000',fontSize: 16, paddingBottom:10}}>Send Feedback</Text>
         </TouchableOpacity>
         <TouchableOpacity>
         <Text style={{color:'#000',fontSize: 16, paddingBottom:10}}>Rate Us on the PlayStore</Text>
         </TouchableOpacity>
         <TouchableOpacity onPress={() => setModalVisible(!modalVisible)}>
         <Text style={{color:'#000',fontSize: 16, paddingBottom:10}}>Log Out</Text>
         </TouchableOpacity>
         </View>
         <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <View style={{position:'absolute',top:0,bottom:0, left:0, right:0, backgroundColor:'rgba(255,255,255,0.9)'}}>
          <View style={{flexDirection:'column', flex:1, justifyContent:'center', alignItems:'center'}}>
          <View style={{backgroundColor:'#fff', width:'70%', paddingVertical:20, paddingHorizontal:20, borderRadius:5,shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
            elevation: 2.2}} >
         <View style={{alignItems:'flex-end'}}>
         </View>
            <Text style={{color:'#89898B'}}>Are you sure want to log out?</Text>
            <View style={{flexDirection:'row', paddingTop:20, justifyContent:'flex-end'}}>
            <TouchableOpacity onPress={() => setModalVisible(!modalVisible)}>
              <Text style={{marginRight:50, color :'#48C2A5', fontSize:12, fontWeight:'bold'}}>CANCEL</Text>
              </TouchableOpacity>
              <Text style={{color :'#48C2A5', fontSize:12, fontWeight:'bold'}}>LOGOUT</Text>
              </View>
          </View>
          </View>
        </View>
      </Modal>
    </SafeAreaView>
    )
}

{/*--------------------------NAV BAR footer------------------------------------------ */}
Profile.navigationOptions={
    headerShown:false
 };

const styles = StyleSheet.create({
    screen:{
        flex:1,
      backgroundColor:'#f8f8f8',       
    }
});

export default Profile;
