 /* -------------------------------------------------------------------------- */
 /*  AUTHOR: FAIZA A
 /* -------------------------------------------------------------------------- */
 /*  DATE: 19th March, 2021.
 /* -------------------------------------------------------------------------- */
 /*  PAGE DESCRIPTION: 
 /*  The page is displays the order tracker and order details. 
 /* -------------------------------------------------------------------------- */
 import React from 'react';
 import { SafeAreaView, StyleSheet, Text, View,Image,TouchableOpacity, Styles, TouchableWithoutFeedback} from 'react-native';
 import Icon from 'react-native-vector-icons/Feather';
 import FontAwesome from 'react-native-vector-icons/FontAwesome';

 const OrderDetails = props=> {
   return (
 
     <SafeAreaView style={styles.screen}>
       
       {/*--------------------------------------------------------------------------- */
        /* ----------------------------BACK BUTTON/ ORDER NUMBER/SUPPORT------------------------------ */}
         
         <View style={{backgroundColor: '#fff', display: 'flex', 
         flexDirection: 'row', paddingTop: '10%', paddingHorizontal:'4%', paddingBottom:10, justifyContent:'space-between',
         borderBottomColor: '#ECECEC',borderBottomWidth: 1, paddingBottom: '3%'}}>
         <View style={{display: 'flex', flexDirection:'row'}}>
         <TouchableOpacity onPress={() => {props.navigation.popToTop()}}>       
         <Icon name="arrow-left" size={22} style={{paddingTop:3,color :'#48C2A5'}}/></TouchableOpacity>
         <Text style={{fontSize: 18, color: '#000000', fontWeight: 'bold', marginLeft:'2%'}}>Order #78782377812</Text>
         </View>
         <View style={{display: 'flex', flexDirection:'row'}}>
         <TouchableOpacity onPress={() => {props.navigation.navigate('Home');}}>       
         <Icon name="globe" size={14} style={{color :'#FF6F6F', paddingTop:'1%'}}/></TouchableOpacity>
         <TouchableOpacity>
         <Text style={{fontSize: 14, color: '#FF6F6F', fontWeight: 'bold', marginLeft:'2%'}}>Support</Text>
         </TouchableOpacity>
         </View>
         </View>
         {/*--------------------------------------------------------------------------- */}
         {/*--------------------------------TRACKER------------------------------------ */}
         {/*--------------------------------------------------------------------------- */}
         <View style={{backgroundColor: '#fff', display: 'flex', 
         flexDirection: 'column', paddingTop: '2%', paddingHorizontal:'4%', paddingBottom:10,
         borderBottomColor: '#ECECEC',borderBottomWidth: 1, paddingBottom: '3%'}}>
         <Text style={{fontSize: 16, color: '#000000', fontWeight: 'bold',paddingBottom: '5%'}}>Order Tracking</Text>
         <View style={{display: 'flex', flexDirection:'row', justifyContent:'space-between',
          paddingBottom: '1%'}}>
         <FontAwesome name="circle" size={12} style={{color :'#48C2A5'}}/>
         <FontAwesome name="circle" size={12} style={{color :'#48C2A5'}}/>
         <FontAwesome name="circle" size={12} style={{color :'#48C2A5'}}/>
         </View>
         <View style={{display: 'flex', flexDirection:'row', justifyContent:'space-between'}}>
         <Text style={{fontSize: 14, color: '#000000'}}>Order accepted</Text>
         <Text style={{fontSize: 14, color: '#000000'}}>Out for delivery</Text>
         <Text style={{fontSize: 14, color: '#000000'}}>Delivered</Text>
         </View>
         </View>

         {/* ----------------------------HEADER DATA START------------------------------ */
         /*                Location and Name of the Pharmacy.               */
         /* --------------------------------------------------------------------------  */}
        
         
         <View style={{display: 'flex', flexDirection:'column',paddingHorizontal: '4%',backgroundColor:'#fff',marginLeft:'1%', paddingTop:'3%'}}>
         <Text style={{fontSize: 18, color: '#000000', fontWeight: 'bold'}}>Faiza Pharmacy</Text>
         <Text style={{fontSize: 12, color: '#a1a1a1'}}>Ballygunge</Text>
         </View>
         {/* ----------------------------ORDER TRACKING -------------------------------------- */}
         {/*------------------------------Items List--------------------------------------------- */}
         <View style={{display:'flex', flexDirection:'column',paddingHorizontal:'5%', 
         paddingVertical:'1%',backgroundColor:'#fff', borderBottomColor: '#ECECEC',
          borderBottomWidth: 2, paddingBottom: '3%'}}>
         <Text style={{fontSize: 14, color:'#b4b4b4', paddingBottom:'1%'}}>Items</Text>
         <View style={{display:'flex', flexDirection:'row',justifyContent:'space-between'}}>
         <View style={{display:'flex', flexDirection:'column'}}>
         <Text style={{fontWeight:'bold', fontSize: 12,color:'#000'}}>Calpol 500 mg Tablet</Text>
         <Text style={{paddingTop:3,fontSize: 12, color:'#b4b4b4'}}>Strip of 15 tablets</Text>
         </View>
         <Text style={{fontSize: 12, color:'#000'}}>₹30.74</Text>
         </View>
         </View>
        
         {/* ----------------------------BILLING DETAILS -------------------------------------- */}
         <View style={{display: 'flex', flexDirection:'column', backgroundColor: '#fff',
         paddingTop:'2%',  paddingHorizontal:'4%'}}>
         <View style={{display: 'flex', flexDirection:'column',borderBottomColor: '#ECECEC', 
         borderBottomWidth: 1, paddingBottom: '5%'}}>
         <Text style={{fontWeight:'bold', fontSize: 14, color:'#010101'}}>Bill Details</Text>
         <View style={{display: 'flex', flexDirection:'row', marginTop:'4%', justifyContent:'space-between'}}>
         <Text style={{fontSize: 12, color:'#000'}}>Item Total</Text>
         <Text style={{paddingTop:'0.2%',fontSize: 12, color:'#000'}}>₹30.74</Text>
         </View>
         <View style={{display: 'flex', flexDirection:'row', paddingBottom:'3%', marginBottom:'2%',borderBottomColor: '#ECECEC', 
         borderBottomWidth: 1, justifyContent:'space-between'}}>
         <Text style={{ fontSize: 12, color:'#4CAEF9'}}>Convenience fee</Text>
         <Text style={{ fontSize: 12, color:'#000'}}>₹35.00</Text>
         </View>
 
         <View style={{display: 'flex', flexDirection:'row', justifyContent:'space-between'}}>
         <Text style={{fontWeight:'bold', fontSize: 12, color:'#010101'}}>Grand Total</Text>
         <Text style={{fontWeight:'bold',fontSize: 12, color:'#010101'}}>₹30.74</Text>
         </View>
         {/*------------------------------SAVINGS MESSAGE--------------------------------------------- */}
         <View style={{display: 'flex', flexDirection:'row',backgroundColor: '#E5F9E1', paddingVertical:10,
         marginTop:'3%',borderWidth:0.7, borderColor:'#50BEA4', borderStyle:'dashed'}}>
         <Text style={{fontWeight:'bold', fontSize: 12, color:'#50BEA4', paddingLeft:'25%' }}>You have saved ₹10 on the bill!</Text>
         </View>
         </View>
         
         {/*------------------------------DELIVERY ADDRESS DETAILS--------------------------------------------- */}
         <View style={{display: 'flex', flexDirection:'column', backgroundColor:'#fff', paddingVertical:'3%'}}>
         <Text style={{fontSize: 12, color:'#3c3c3c',paddingBottom:'2%'}}>Delivery Address </Text>
         <Text style={{fontSize: 12, color:'#a1a1a1', width:'80%'}}>Walden White,</Text>
         <Text style={{fontSize: 12, color:'#a1a1a1', width:'80%'}}>171D/1H Picnic Garden Road,</Text>
         <Text style={{fontSize: 12, color:'#a1a1a1', width:'80%'}}>Kolkata, West Bengal - 700039.</Text>
         <Text style={{fontSize: 12, color:'#a1a1a1', width:'80%'}}>Mobile: +91 9078047092</Text>
         </View>
         {/*------------------------------DELIVERY ADDRESS ENDS--------------------------------------------- */}
         {/*------------------------------GET IN TOUCH------------------------------------------------------ */}
         {/*------------------------------disappears when the order is delivered---------------------------- */}
         <View style={{display: 'flex', flexDirection:'row'}}>
          <TouchableOpacity>
          <FontAwesome name='phone' size={15} style={{color :'#FF6F6F', padding:5}}/>
          </TouchableOpacity>
          <TouchableOpacity>
          <Text style={{marginLeft:3, color:'#FF6F6F',fontSize: 14}}>Get in touch with the pharmacy</Text>
          </TouchableOpacity>
          </View>
         {/*------------------------------GET IN TOUCH ENDS------------------------------------------------------ */}
        
         </View>
        </SafeAreaView>
 
   /* ---------------------------------- END ----------------------------------- */
  
   );
 }
 OrderDetails.navigationOptions={
     headerShown:false
  };
 
 const styles = StyleSheet.create({
     screen:{
       flex:1,
       backgroundColor:'#f8f8f8',       
     }
 });
 export default OrderDetails;