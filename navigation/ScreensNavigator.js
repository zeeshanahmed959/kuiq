import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';
import Login from '../Screens/Login';
import Splash from '../Screens/Splash';
import Otp from '../Screens/Otp';
import Signup from '../Screens/Signup';
import Home from '../Screens/Home';
import Store from '../Screens/Store';
import NavigationBar from '../components/NavigationBar';
import Cart from '../Screens/Cart';
import Order from '../Screens/Order';
import Profile from '../Screens/Profile';
import Search from '../Screens/Search';
import Search2 from '../Screens/Search2';
import OrderDetails from '../Screens/OrderDetails'
import AddLocation from '../Screens/AddLocation'
import PaymentMethod from '../Screens/PaymentMethod';
import TermsOfService from '../Screens/TermsOfService';
import Address from '../Screens/Address';
import AddressDetails from '../Screens/AddressDetails';
import Coupon from '../Screens/Coupon';
import Faq from '../Screens/Faq';
import Prescriptions from '../Screens/Prescriptions';
import PrescriptionView from '../Screens/PrescriptionView';
import About from '../Screens/About';
import Feedback from '../Screens/Feedback';

const ScreensNavigator = createStackNavigator({
    Splash:Splash,
    NavigationBar: NavigationBar, 
    Feedback: Feedback, 
    PrescriptionView:PrescriptionView,
    Prescriptions:Prescriptions,
    Coupon: Coupon,
    Address:Address,
    Faq: Faq,
    AddressDetails: AddressDetails,
    TermsOfService:TermsOfService,
    NavigationBar: NavigationBar, 
    PaymentMethod:PaymentMethod,
    Search:Search,
    Search2:Search2,
    Cart: Cart,
    Login:Login,
    Otp:Otp,
    Signup:Signup, 
    Home:Home,
    Store:Store,
    Profile:Profile,
    Order:Order,
    OrderDetails:OrderDetails,
    Search2:Search2,
    AddLocation:AddLocation,
    TermsOfService:TermsOfService,
});

export default createAppContainer(ScreensNavigator);